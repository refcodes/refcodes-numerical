// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

import org.refcodes.mixin.ValueAccessor.ValueBuilder;
import org.refcodes.mixin.ValueAccessor.ValueProperty;

/**
 * The bitwise operation builder applies bitwise operations to a given value
 * being set via <code>setValue(Integer)</code> or (as of the Builder-Pattern)
 * {@link #withValue(Integer)}.
 */
public class BitwiseOperationBuilder implements ValueProperty<Integer>, ValueBuilder<Integer, BitwiseOperationBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Integer _value = 0;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getValue() {
		return _value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setValue( Integer aValue ) {
		_value = aValue;
	}

	/**
	 * Applies a bitwise AND operation to the value property with the given
	 * operand; the value is updated accordingly and the result can be retrieved
	 * with the {@link #getValue()} method.
	 * 
	 * @param aOperand The operand to be applied to the value property.
	 * 
	 * @return The {@link BitwiseOperationBuilder} as of the Builder-Pattern to
	 *         chain multiple operations.
	 */
	public BitwiseOperationBuilder withAnd( Integer aOperand ) {
		_value = toBitwiseAND( _value, aOperand );
		return this;
	}

	/**
	 * Applies a bitwise OR operation to the value property with the given
	 * operand; the value is updated accordingly and the result can be retrieved
	 * with the {@link #getValue()} method.
	 * 
	 * @param aOperand The operand to be applied to the value property.
	 * 
	 * @return The {@link BitwiseOperationBuilder} as of the Builder-Pattern to
	 *         chain multiple operations.
	 */
	public BitwiseOperationBuilder withOr( Integer aOperand ) {
		_value = toBitwiseOR( _value, aOperand );
		return this;
	}

	/**
	 * Applies a bitwise XOR operation to the value property with the given
	 * operand; the value is updated accordingly and the result can be retrieved
	 * with the {@link #getValue()} method.
	 * 
	 * @param aOperand The operand to be applied to the value property.
	 * 
	 * @return The {@link BitwiseOperationBuilder} as of the Builder-Pattern to
	 *         chain multiple operations.
	 */
	public BitwiseOperationBuilder withXor( Integer aOperand ) {
		_value = toBitwiseXOR( _value, aOperand );
		return this;
	}

	/**
	 * Applies a bitwise NOT operation to the value property ; the value is
	 * updated accordingly and the result can be retrieved with the
	 * {@link #getValue()} method.
	 * 
	 * @return The {@link BitwiseOperationBuilder} as of the Builder-Pattern to
	 *         chain multiple operations.
	 */
	public BitwiseOperationBuilder withNot() {
		_value = toBitwiseNOT( _value );
		return this;
	}

	/**
	 * Tests if the bits set in the mask argument are also set in the value
	 * property ("the value property is maskable with the given mask"). If this
	 * is the case true is returned. Else false is returned. Additionally bits
	 * set in the value property are not considered in the result. The
	 * bit-length of the mask may not be the same as the bit-length of the
	 * value.
	 * 
	 * @param aMask The mask which's true bits are to be verified against the
	 *        value property.
	 * 
	 * @return True in case the value property is maskable by the given mask.
	 */
	public boolean isMaskable( int aMask ) {
		return BitwiseOperationBuilder.isMaskable( _value, aMask );
	}

	/**
	 * Tests f the value is true at the given position.
	 *
	 * @param aPosition The bit position to test.
	 * 
	 * @return True, if the value is true at the given position.
	 */
	public boolean isTrueAt( int aPosition ) {
		return BitwiseOperationBuilder.isTrueAt( _value, aPosition );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BitwiseOperationBuilder withValue( Integer aValue ) {
		setValue( aValue );
		return this;
	}

	/**
	 * Tests whether the bit at the given position is set in the parameter value
	 * and returns true if this is the case. Else false is returned. Also
	 * Returns false if an index out of range is provided. CATION: This method
	 * may not be very optimized!
	 * 
	 * @param aValue The value interpreted as a bit-field.
	 * @param aPos The position of the bit-field to be tested.
	 * 
	 * @return True in case the bit in the bit-field at the given position is
	 *         true.
	 */
	public static boolean isTrueAt( int aValue, int aPos ) {
		final String bitString = Integer.toBinaryString( aValue );
		final int bitStringLength = bitString.length();
		if ( ( aPos < 0 ) || ( aPos > bitStringLength - 1 ) ) {
			return false;
		}
		if ( bitString.charAt( bitStringLength - 1 - aPos ) == '1' ) {
			return true;
		}
		return false;
	}

	/**
	 * Tests if the bits set in the parameter mask are also set in the parameter
	 * value. If this is the case true is returned. Else false is returned.
	 * Additionally bits set in the parameter value are not considered in the
	 * result. The bit-length of the mask may not be the same as the bit-length
	 * of the value. CATION: This method may not be very optimized!
	 * 
	 * @param aValue The value to see if the true values in the mask are also
	 *        true in the value.
	 * @param aMask The mask which's true values are to be verified against the
	 *        provided value.
	 * 
	 * @return Description is currently not available!
	 */
	public static boolean isMaskable( int aValue, int aMask ) {
		final String[] bitStrings = BitwiseOperationBuilder.toBitStrings( aValue, aMask );
		for ( int i = 0; i < bitStrings[0].length(); i++ ) {
			if ( ( bitStrings[1].charAt( i ) == '1' ) && ( bitStrings[0].charAt( i ) == '0' ) ) {
				return false;
			}
		}
		return true;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The result of this method is a contransceiver of the parameters valueA
	 * and valueB. The operation is a bitwise logical AND.
	 * 
	 * @param aValueA Value A to be used.
	 * @param aValueB Value B to be used.
	 * 
	 * @return The boolean result of the bitwise AND.
	 */
	protected static int toBitwiseAND( int aValueA, int aValueB ) {
		String result = "";
		final String[] bitStrings = toBitStrings( aValueA, aValueB );

		for ( int i = 0; i < bitStrings[0].length(); i++ ) {
			if ( ( bitStrings[0].charAt( i ) == '1' ) && ( bitStrings[1].charAt( i ) == '1' ) ) {
				result += "1";
			}
			else {
				result += "0";
			}
		}

		return Integer.parseInt( result, 2 );
	}

	/**
	 * The result of this method is the negation of the given value. The
	 * operation is a bitwise logical NOT.
	 * 
	 * @param aValue Value to be used.
	 * 
	 * @return The boolean result of the bitwise NOT.
	 */
	protected static int toBitwiseNOT( int aValue ) {
		String result = "";
		final String bitString = Integer.toBinaryString( aValue );
		final int strLenght = bitString.length();

		for ( int i = 0; i < strLenght; i++ ) {
			result += bitString.charAt( i ) == '1' ? "0" : "1";
		}

		return Integer.parseInt( result, 2 );
	}

	/**
	 * The result of this method is a distransceiver of the parameters valueA
	 * and valueB. The operation is a bitwise logical OR.
	 * 
	 * @param aValueA Value A to be used.
	 * @param aValueB Value B to be used.
	 * 
	 * @return The boolean result of the bitwise OR.
	 */
	protected static int toBitwiseOR( int aValueA, int aValueB ) {
		String result = "";
		final String[] bitStrings = toBitStrings( aValueA, aValueB );

		for ( int i = 0; i < bitStrings[0].length(); i++ ) {
			if ( ( bitStrings[0].charAt( i ) == '1' ) || ( bitStrings[1].charAt( i ) == '1' ) ) {
				result += "1";
			}
			else {
				result += "0";
			}
		}

		return Integer.parseInt( result, 2 );
	}

	/**
	 * The result of this method is an exclusive distransceiver of the
	 * parameters valueA and valueB. The operation is a bitwise logical XOR.
	 * 
	 * @param aValueA Value A to be used.
	 * @param aValueB Value B to be used.
	 * 
	 * @return The boolean result of the bitwise XOR.
	 */
	protected static int toBitwiseXOR( int aValueA, int aValueB ) {
		String result = "";
		final String[] bitStrings = toBitStrings( aValueA, aValueB );

		for ( int i = 0; i < bitStrings[0].length(); i++ ) {
			if ( ( bitStrings[0].charAt( i ) == '1' ) ^ ( bitStrings[1].charAt( i ) == '1' ) ) {
				result += "1";
			}
			else {
				result += "0";
			}
		}

		return Integer.parseInt( result, 2 );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns an array of binary strings as a representation of the integer
	 * input values in base 2.
	 *
	 * @param aValueA The first value for which to get the string.
	 * @param aValueB The second value for which to get the string.
	 * 
	 * @return The array with the strings containing the according bit-fields.
	 */
	protected static String[] toBitStrings( int aValueA, int aValueB ) {
		String theBitStringA = Integer.toBinaryString( aValueA );
		String theBitStringB = Integer.toBinaryString( aValueB );
		if ( theBitStringA.length() > theBitStringB.length() ) {
			while ( theBitStringB.length() < theBitStringA.length() ) {
				theBitStringB = "0" + theBitStringB;
			}
		}
		else {
			if ( theBitStringA.length() < theBitStringB.length() ) {
				while ( theBitStringA.length() < theBitStringB.length() ) {
					theBitStringA = "0" + theBitStringA;
				}
			}
		}
		return new String[] { theBitStringA, theBitStringB };
	}
}
