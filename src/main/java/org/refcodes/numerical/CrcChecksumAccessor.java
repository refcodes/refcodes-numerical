// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

/**
 * Provides an accessor for a CRC checksum property.
 */
public interface CrcChecksumAccessor {

	/**
	 * Retrieves the CRC checksum from the CRC checksum property.
	 * 
	 * @return The CRC checksum stored by the CRC checksum property.
	 */
	long getCrcChecksum();

	/**
	 * Provides a mutator for a CRC checksum property.
	 */
	public interface CrcChecksumMutator {

		/**
		 * Sets the CRC checksum for the CRC checksum property.
		 * 
		 * @param aCrcChecksum The CRC checksum to be stored by the CRC checksum
		 *        property.
		 */
		void setCrcChecksum( long aCrcChecksum );
	}

	/**
	 * Provides a builder method for a CRC checksum property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CrcChecksumBuilder<B extends CrcChecksumBuilder<B>> {

		/**
		 * Sets the CRC checksum for the CRC checksum property.
		 * 
		 * @param aCrcChecksum The CRC checksum to be stored by the CRC checksum
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCrcChecksum( long aCrcChecksum );
	}

	/**
	 * Provides a CRC checksum property.
	 */
	public interface CrcChecksumProperty extends CrcChecksumAccessor, CrcChecksumMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given long (setter) as of
		 * {@link #setCrcChecksum(long)} and returns the very same value
		 * (getter).
		 * 
		 * @param aCrcChecksum The long to set (via
		 *        {@link #setCrcChecksum(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letCrcChecksum( long aCrcChecksum ) {
			setCrcChecksum( aCrcChecksum );
			return aCrcChecksum;
		}
	}
}
