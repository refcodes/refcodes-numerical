// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

/**
 * Provides an accessor for a {@link CrcAlgorithm} property.
 */
public interface CrcAlgorithmAccessor {

	/**
	 * Retrieves the value from the {@link CrcAlgorithm} property.
	 * 
	 * @return The name stored by the {@link CrcAlgorithm} property.
	 */
	CrcAlgorithm getCrcAlgorithm();

	/**
	 * Provides a mutator for a {@link CrcAlgorithm} property.
	 */
	public interface CrcAlgorithmMutator {

		/**
		 * Sets the value for the {@link CrcAlgorithm} property.
		 * 
		 * @param aCrcAlgorithm The value to be stored by the
		 *        {@link CrcAlgorithm} property.
		 */
		void setCrcAlgorithm( CrcAlgorithm aCrcAlgorithm );
	}

	/**
	 * Provides a builder method for a {@link CrcAlgorithm} property returning
	 * the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CrcAlgorithmBuilder<B extends CrcAlgorithmBuilder<B>> {

		/**
		 * Sets the value for the {@link CrcAlgorithm} property.
		 * 
		 * @param aCrcAlgorithm The value to be stored by the
		 *        {@link CrcAlgorithm} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCrcAlgorithm( CrcAlgorithm aCrcAlgorithm );
	}

	/**
	 * Provides a {@link CrcAlgorithm} property.
	 */
	public interface CrcAlgorithmProperty extends CrcAlgorithmAccessor, CrcAlgorithmMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link CrcAlgorithm}
		 * (setter) as of {@link #setCrcAlgorithm(CrcAlgorithm)} and returns the
		 * very same value (getter).
		 * 
		 * @param aCrcAlgorithm The {@link CrcAlgorithm} to set (via
		 *        {@link #setCrcAlgorithm(CrcAlgorithm)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default CrcAlgorithm letCrcAlgorithm( CrcAlgorithm aCrcAlgorithm ) {
			setCrcAlgorithm( aCrcAlgorithm );
			return aCrcAlgorithm;
		}
	}
}
