// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

import org.refcodes.exception.UnhandledEnumBugException;

/**
 * An enumeration of valid endianess.
 */
public enum Endianess {

	LITTLE, BIG;

	/**
	 * Converts the given value to a byte array in the according endian
	 * notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * @param aLength The number of bytes to use.
	 * 
	 * @return The according array.
	 */
	public byte[] toBytes( long aValue, int aLength ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toBigEndianBytes( aValue, aLength );
		case LITTLE -> NumericalUtility.toLittleEndianBytes( aValue, aLength );
		};
	}

	/**
	 * Converts the given value to a byte array in the according endian
	 * notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public byte[] toBytes( long aValue ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toBigEndianBytes( aValue );
		case LITTLE -> NumericalUtility.toLittleEndianBytes( aValue );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts the given value to a byte array in the according endian
	 * notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * @param aLength The number of bytes to use.
	 * 
	 * @return The according array.
	 */
	public byte[] toBytes( int aValue, int aLength ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toBigEndianBytes( aValue, aLength );
		case LITTLE -> NumericalUtility.toLittleEndianBytes( aValue, aLength );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts the given value to a byte array in the according endian
	 * notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public byte[] toBytes( int aValue ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toBigEndianBytes( aValue );
		case LITTLE -> NumericalUtility.toLittleEndianBytes( aValue );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts the given value to a byte array in the according endian
	 * notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * @param aLength The number of bytes to use.
	 * 
	 * @return The according array.
	 */
	public byte[] toBytes( short aValue, int aLength ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toBigEndianBytes( aValue, aLength );
		case LITTLE -> NumericalUtility.toLittleEndianBytes( aValue, aLength );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts the given value to a byte array in the according endian
	 * notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public byte[] toBytes( short aValue ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toBigEndianBytes( aValue );
		case LITTLE -> NumericalUtility.toLittleEndianBytes( aValue );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts the given value to a byte array in the according endian
	 * notation. As the float type is 32 bits in java, an array of length 4 will
	 * be returned.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public byte[] toBytes( float aValue ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toBigEndianBytes( aValue );
		case LITTLE -> NumericalUtility.toLittleEndianBytes( aValue );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts the given value to a byte array in the according endian
	 * notation. As the float type is 32 bits in java, an array of length 4 will
	 * be returned.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public byte[] toBytes( double aValue ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toBigEndianBytes( aValue );
		case LITTLE -> NumericalUtility.toLittleEndianBytes( aValue );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts the given value to a byte array in the according endian
	 * notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * @param aLength The number of bytes to use.
	 * 
	 * @return The according array.
	 */
	public byte[] toUnsignedBytes( long aValue, int aLength ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toUnsignedBigEndianBytes( aValue, aLength );
		case LITTLE -> NumericalUtility.toUnsignedLittleEndianBytes( aValue, aLength );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts the given value to a byte array in the according endian
	 * notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public byte[] toUnsignedBytes( long aValue ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toUnsignedBigEndianBytes( aValue );
		case LITTLE -> NumericalUtility.toUnsignedLittleEndianBytes( aValue );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts the given value to a byte array in the according endian
	 * notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public byte[] toUnsignedBytes( int aValue ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toUnsignedBigEndianBytes( aValue );
		case LITTLE -> NumericalUtility.toUnsignedLittleEndianBytes( aValue );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts the given value to a byte array in the according endian
	 * notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public byte[] toUnsignedBytes( short aValue ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toUnsignedBigEndianBytes( aValue );
		case LITTLE -> NumericalUtility.toUnsignedLittleEndianBytes( aValue );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts a byte array in according endianess to a long value.
	 * 
	 * @param aBytes The byte array to be converted.
	 * 
	 * @return The resulting long value.
	 */
	public long toLong( byte[] aBytes ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toLongFromBigEndianBytes( aBytes );
		case LITTLE -> NumericalUtility.toLongFromLittleEndianBytes( aBytes );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts a byte array in according endianess to an unsigned long value.
	 * 
	 * @param aBytes The byte array to be converted.
	 * 
	 * @return The resulting long value.
	 */
	public long toUnsignedLong( byte[] aBytes ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toUnsignedLongFromBigEndianBytes( aBytes );
		case LITTLE -> NumericalUtility.toUnsignedLongFromLittleEndianBytes( aBytes );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts a byte array in according endianess to an integer value.
	 * 
	 * @param aBytes The byte array to be converted.
	 * 
	 * @return The resulting integer value.
	 */
	public int toInteger( byte[] aBytes ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toIntFromBigEndianBytes( aBytes );
		case LITTLE -> NumericalUtility.toIntFromLittleEndianBytes( aBytes );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts a byte array in according endianess to an unsigned integer
	 * value.
	 * 
	 * @param aBytes The byte array to be converted.
	 * 
	 * @return The resulting integer value.
	 */
	public int toUnsignedInteger( byte[] aBytes ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toUnsignedIntFromBigEndianBytes( aBytes );
		case LITTLE -> NumericalUtility.toUnsignedIntFromLittleEndianBytes( aBytes );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts a byte array in according endianess to an short value.
	 * 
	 * @param aBytes The byte array to be converted.
	 * 
	 * @return The resulting short value.
	 */
	public short toShort( byte[] aBytes ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toShortFromBigEndianBytes( aBytes );
		case LITTLE -> NumericalUtility.toShortFromLittleEndianBytes( aBytes );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts a byte array in according endianess to an unsigned short value.
	 * 
	 * @param aBytes The byte array to be converted.
	 * 
	 * @return The resulting short value.
	 */
	public short toUnsignedShort( byte[] aBytes ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toUnsignedShortFromBigEndianBytes( aBytes );
		case LITTLE -> NumericalUtility.toUnsignedShortFromLittleEndianBytes( aBytes );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts a byte array in according endianess to an float value.
	 * 
	 * @param aBytes The byte array to be converted.
	 * 
	 * @return The resulting float value.
	 */
	public float toFloat( byte[] aBytes ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toFloatFromBigEndianBytes( aBytes );
		case LITTLE -> NumericalUtility.toFloatFromLittleEndianBytes( aBytes );
		default -> throw new UnhandledEnumBugException( this );
		};
	}

	/**
	 * Converts a byte array in according endianess to an double value.
	 * 
	 * @param aBytes The byte array to be converted.
	 * 
	 * @return The resulting double value.
	 */
	public double toDouble( byte[] aBytes ) {
		return switch ( this ) {
		case BIG -> NumericalUtility.toDoubleFromBigEndianBytes( aBytes );
		case LITTLE -> NumericalUtility.toDoubleFromLittleEndianBytes( aBytes );
		default -> throw new UnhandledEnumBugException( this );
		};
	}
}
