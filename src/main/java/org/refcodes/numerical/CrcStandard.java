// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

// -----------------------------------------------------------------------------
// The following note is provided as of the code in `CrcAlgorithm` has been
// copy'n'pasted from the "Online CRC-8 CRC-16 CRC-32 Calculator"
// (https://crccalc.com)'s GitHub "https://github.com/meetanthony/crcjava.git"
// repository:
// -----------------------------------------------------------------------------

package org.refcodes.numerical;

/**
 * Enumeration with CRC implementations hacked together from sources out there.
 */
public enum CrcStandard implements CrcAlgorithm {

	// @formatter:off
	CRC_8("CRC-8", 8, 0xD5, 0x0, false, false, 0x0),
	CRC_8_ARDUINO("CRC-8/ARDUINO", 8, 0x8C, 0x0, false, false, 0x0),
	CRC_8_CCITT("CRC-8/CCITT", 8, 0x7, 0x0, false, false, 0x0),
    CRC_8_CDMA2000("CRC-8/CDMA2000", 8, 0x9B, 0xFF, false, false, 0x0),
    CRC_8_DARC("CRC-8/DARC", 8, 0x39, 0x0, true, true, 0x0),
    CRC_8_DVBS2("CRC-8/DVB-S2", 8, 0xD5, 0x0, false, false, 0x0),
    CRC_8_SAE_J1850("CRC-8/SAE_J1850", 8, 0x1D, 0xFF, true, true, 0x0),
    CRC_8_EBU("CRC-8/EBU", 8, 0x1D, 0xFF, true, true, 0x0),
    CRC_8_ICODE("CRC-8/I-CODE", 8, 0x1D, 0xFD, false, false, 0x0),
    CRC_8_ITU("CRC-8/ITU", 8, 0x7, 0x0, false, false, 0x55),
    CRC_8_MAXIM("CRC-8/MAXIM", 8, 0x31, 0x0, true, true, 0x0),
    CRC_8_ROHC("CRC-8/ROHC", 8, 0x7, 0xFF, true, true, 0x0),
    CRC_8_WCDMA("CRC-8/WCDMA", 8, 0x9B, 0x0, true, true, 0x0),
	CRC_16_CCITT_FALSE("CRC-16/CCITT-FALSE", 16, 0x1021, 0xFFFF, false, false, 0x0),
    CRC_16_ARC("CRC-16/ARC", 16, 0x8005, 0x0, true, true, 0x0),
    CRC_16_AUG_CCITT("CRC-16/AUG-CCITT", 16, 0x1021, 0x1D0F, false, false, 0x0),
    CRC_16_BUYPASS("CRC-16/BUYPASS", 16, 0x8005, 0x0, false, false, 0x0),
    CRC_16_CDMA2000("CRC-16/CDMA2000", 16, 0xC867, 0xFFFF, false, false, 0x0),
    CRC_16_DDS_110("CRC-16/DDS-110", 16, 0x8005, 0x800D, false, false, 0x0),
    CRC_16_DECT_R("CRC-16/DECT-R", 16, 0x589, 0x0, false, false, 0x1),
    CRC_16_DECT_X("CRC-16/DECT-X", 16, 0x589, 0x0, false, false, 0x0),
    CRC_16_DNP("CRC-16/DNP", 16, 0x3D65, 0x0, true, true, 0xFFFF),
    CRC_16_EN_13757("CRC-16/EN-13757", 16, 0x3D65, 0x0, false, false, 0xFFFF),
    CRC_16_GENIBUS("CRC-16/GENIBUS", 16, 0x1021, 0xFFFF, false, false, 0xFFFF),
    CRC_16_MAXIM("CRC-16/MAXIM", 16, 0x8005, 0x0, true, true, 0xFFFF),
    CRC_16_MCRF4XX("CRC-16/MCRF4XX", 16, 0x1021, 0xFFFF, true, true, 0x0),
    CRC_16_RIELLO("CRC-16/RIELLO", 16, 0x1021, 0xB2AA, true, true, 0x0),
    CRC_16_T10_DIF("CRC-16/T10-DIF", 16, 0x8BB7, 0x0, false, false, 0x0),
    CRC_16_TELEDISK("CRC-16/TELEDISK", 16, 0xA097, 0x0, false, false, 0x0),
    CRC_16_TMS37157("CRC-16/TMS37157", 16, 0x1021, 0x89EC, true, true, 0x0),
    CRC_16_USB("CRC-16/USB", 16, 0x8005, 0xFFFF, true, true, 0xFFFF),
    CRC_16_A("CRC-16/A", 16, 0x1021, 0xc6c6, true, true, 0x0),
    CRC_16_KERMIT("CRC-16/KERMIT", 16, 0x1021, 0x0, true, true, 0x0),
    CRC_16_MODBUS("CRC-16/MODBUS", 16, 0x8005, 0xFFFF, true, true, 0x0),
    CRC_16_X25("CRC-16/X-25", 16, 0x1021, 0xFFFF, true, true, 0xFFFF),
    CRC_16_XMODEM("CRC-16/XMODEM", 16, 0x1021, 0x0, false, false, 0x0),
    CRC_32("CRC-32", 32, 0x04C11DB7L, 0xFFFFFFFFL, true, true, 0xFFFFFFFFL),
    CRC_32_BZIP2("CRC-32/BZIP2", 32, 0x04C11DB7L, 0xFFFFFFFFL, false, false, 0xFFFFFFFFL),
    CRC_32_C("CRC-32/C", 32, 0x1EDC6F41L, 0xFFFFFFFFL, true, true, 0xFFFFFFFFL),
    CRC_32_D("CRC-32/D", 32, 0xA833982BL, 0xFFFFFFFFL, true, true, 0xFFFFFFFFL),
    CRC_32_JAMCRC("CRC-32/JAMCRC", 32, 0x04C11DB7L, 0xFFFFFFFFL, true, true, 0x00000000L),
    CRC_32_MPEG2("CRC-32/MPEG-2", 32, 0x04C11DB7L, 0xFFFFFFFFL, false, false, 0x00000000L),
    CRC_32_POSIX("CRC-32/POSIX", 32, 0x04C11DB7L, 0x00000000L, false, false, 0xFFFFFFFFL),
    CRC_32_Q("CRC-32/Q", 32, 0x814141ABL, 0x00000000L, false, false, 0x00000000L),
    CRC_32_XFER("CRC-32/XFER", 32, 0x000000AFL, 0x00000000L, false, false, 0x00000000L),
    CRC_64("CRC-64",64, 0x42F0E1EBA9EA3693L, 0x00000000L, false, false, 0x00000000L),
    CRC_64_WE("CRC-64/WE", 64, 0x42F0E1EBA9EA3693L, 0xFFFFFFFFFFFFFFFFL, false, false, 0xFFFFFFFFFFFFFFFFL),
    CRC_64_XZ("CRC-64/XZ", 64, 0x42F0E1EBA9EA3693L, 0xFFFFFFFFFFFFFFFFL, true, true, 0xFFFFFFFFFFFFFFFFL);
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private CrcAlgorithm _crcAlgorithm;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private CrcStandard( String aName, int aCrcBitWidth, long aPolynomial, long aInitCrc, boolean aRefIn, boolean aRefOut, long aXorOut ) {
		_crcAlgorithm = new CrcAlgorithmImpl( aName, aCrcBitWidth, aPolynomial, aInitCrc, aRefIn, aRefOut, aXorOut );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( byte aData ) {
		return _crcAlgorithm.toCrcChecksum( aData );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return _crcAlgorithm.getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCrcWidth() {
		return _crcAlgorithm.getCrcWidth();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( long aCrc, byte aData ) {
		return _crcAlgorithm.toCrcChecksum( aCrc, aData );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( byte[] aData ) {
		return _crcAlgorithm.toCrcChecksum( aData );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( long aCrc, byte[] aData ) {
		return _crcAlgorithm.toCrcChecksum( aCrc, aData );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( byte[] aData, int aOffset, int aLength ) {
		return _crcAlgorithm.toCrcChecksum( aData, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( long aCrc, byte[] aData, int aOffset, int aLength ) {
		return _crcAlgorithm.toCrcChecksum( aCrc, aData, aOffset, aLength );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( byte aData, Endianess aEndianess ) {
		return _crcAlgorithm.toCrcBytes( aData, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( long aCrc, byte aData, Endianess aEndianess ) {
		return _crcAlgorithm.toCrcBytes( aCrc, aData, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( byte[] aData, Endianess aEndianess ) {
		return _crcAlgorithm.toCrcBytes( aData, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( long aCrc, byte[] aData, Endianess aEndianess ) {
		return _crcAlgorithm.toCrcBytes( aCrc, aData, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( byte[] aData, int aOffset, int aLength, Endianess aEndianess ) {
		return _crcAlgorithm.toCrcBytes( aData, aOffset, aLength, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( long aCrc, byte[] aData, int aOffset, int aLength, Endianess aEndianess ) {
		return _crcAlgorithm.toCrcBytes( aCrc, aData, aOffset, aLength, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CrcSize getCrcSize() {
		return _crcAlgorithm.getCrcSize();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return _crcAlgorithm.getName();
	}
}