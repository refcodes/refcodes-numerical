// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

/**
 * Provides an accessor for a CRC mode property.
 */
public interface ChecksumValidationModeAccessor {

	/**
	 * Retrieves the validation mode from the validation mode property.
	 * 
	 * @return The validation mode stored by the validation mode property.
	 */
	ChecksumValidationMode getChecksumValidationMode();

	/**
	 * Provides a mutator for a validation mode property.
	 */
	public interface ChecksumValidationModeMutator {

		/**
		 * Sets the validation mode for the validation mode property.
		 * 
		 * @param aChecksumValidationMode The validation mode to be stored by
		 *        the validation mode property.
		 */
		void setChecksumValidationMode( ChecksumValidationMode aChecksumValidationMode );
	}

	/**
	 * Provides a builder method for a validation mode property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ChecksumValidationModeBuilder<B extends ChecksumValidationModeBuilder<B>> {

		/**
		 * Sets the validation mode for the validation mode property.
		 * 
		 * @param aChecksumValidationMode The validation mode to be stored by
		 *        the validation mode property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withChecksumValidationMode( ChecksumValidationMode aChecksumValidationMode );
	}

	/**
	 * Provides a validation mode property.
	 */
	public interface ChecksumValidationModeProperty extends ChecksumValidationModeAccessor, ChecksumValidationModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link ChecksumValidationMode} (setter) as of
		 * {@link #setChecksumValidationMode(ChecksumValidationMode)} and
		 * returns the very same value (getter).
		 * 
		 * @param aChecksumValidationMode The {@link ChecksumValidationMode} to
		 *        set (via
		 *        {@link #setChecksumValidationMode(ChecksumValidationMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ChecksumValidationMode letChecksumMode( ChecksumValidationMode aChecksumValidationMode ) {
			setChecksumValidationMode( aChecksumValidationMode );
			return aChecksumValidationMode;
		}
	}
}
