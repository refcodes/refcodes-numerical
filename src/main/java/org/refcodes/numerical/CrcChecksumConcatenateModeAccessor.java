// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

import org.refcodes.mixin.ConcatenateMode;

/**
 * Provides an accessor for a {@link ConcatenateMode} property.
 */
public interface CrcChecksumConcatenateModeAccessor {

	/**
	 * Retrieves the {@link ConcatenateMode} from the CRC checksum
	 * {@link ConcatenateMode} property.
	 * 
	 * @return The {@link ConcatenateMode} stored by the CRC checksum
	 *         {@link ConcatenateMode} property.
	 */
	ConcatenateMode getCrcChecksumConcatenateMode();

	/**
	 * Provides a mutator for a CRC checksum {@link ConcatenateMode} property.
	 */
	public interface CrcChecksumConcatenateModeMutator {

		/**
		 * Sets the {@link ConcatenateMode} for the {@link ConcatenateMode}
		 * property.
		 * 
		 * @param aCrcChecksumConcatenateMode The {@link ConcatenateMode} to be
		 *        stored by the CRC checksum {@link ConcatenateMode} property.
		 */
		void setCrcChecksumConcatenateMode( ConcatenateMode aCrcChecksumConcatenateMode );
	}

	/**
	 * Provides a builder method for a CRC checksum {@link ConcatenateMode}
	 * property returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CrcChecksumConcatenateModeBuilder<B extends CrcChecksumConcatenateModeBuilder<B>> {

		/**
		 * Sets the {@link ConcatenateMode} for the CRC checksum
		 * {@link ConcatenateMode} property.
		 * 
		 * @param aCrcChecksumConcatenateMode The {@link ConcatenateMode} to be
		 *        stored by the CRC checksum {@link ConcatenateMode} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCrcChecksumConcatenateMode( ConcatenateMode aCrcChecksumConcatenateMode );
	}

	/**
	 * Provides a CRC checksum {@link ConcatenateMode} property.
	 */
	public interface CrcChecksumConcatenateModeProperty extends CrcChecksumConcatenateModeAccessor, CrcChecksumConcatenateModeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link ConcatenateMode}
		 * (setter) as of
		 * {@link #setCrcChecksumConcatenateMode(ConcatenateMode)} and returns
		 * the very same value (getter).
		 * 
		 * @param aCrcChecksumConcatenateMode The {@link ConcatenateMode} to set
		 *        (via {@link #setCrcChecksumConcatenateMode(ConcatenateMode)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ConcatenateMode letCrcChecksumConcatenateMode( ConcatenateMode aCrcChecksumConcatenateMode ) {
			setCrcChecksumConcatenateMode( aCrcChecksumConcatenateMode );
			return aCrcChecksumConcatenateMode;
		}
	}
}
