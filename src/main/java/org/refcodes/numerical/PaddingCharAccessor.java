// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

/**
 * Provides an accessor for a padding char property.
 */
public interface PaddingCharAccessor {

	/**
	 * Retrieves the value from the padding char property.
	 * 
	 * @return The name stored by the padding char property.
	 */
	char getPaddingChar();

	/**
	 * Provides a mutator for a padding char property.
	 */
	public interface PaddingCharMutator {

		/**
		 * Sets the value for the padding char property.
		 * 
		 * @param aPaddingChar The value to be stored by the padding char
		 *        property.
		 */
		void setPaddingChar( char aPaddingChar );
	}

	/**
	 * Provides a builder method for a padding char property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PaddingCharBuilder<B extends PaddingCharBuilder<B>> {

		/**
		 * Sets the value for the padding char property.
		 * 
		 * @param aPaddingChar The value to be stored by the padding char
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPaddingChar( char aPaddingChar );
	}

	/**
	 * Provides a padding char property.
	 */
	public interface PaddingCharProperty extends PaddingCharAccessor, PaddingCharMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given character (setter) as of
		 * {@link #setPaddingChar(char)} and returns the very same value
		 * (getter).
		 * 
		 * @param aPaddingChar The character to set (via
		 *        {@link #setPaddingChar(char)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default char letPaddingChar( char aPaddingChar ) {
			setPaddingChar( aPaddingChar );
			return aPaddingChar;
		}
	}
}
