// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

/**
 * The {@link InvertibleComposite} implements the {@link Invertible} and is
 * composed of a {@link BijectiveFunction} and the according
 * {@link InverseFunction}.
 * 
 * @param <B> The type of the bijective function's result (being the inverse
 *        function's input type).
 * @param <I> The type of the inverse function's result (being the bijective
 *        function's input type).
 */
public class InvertibleComposite<B, I> implements Invertible<B, I> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final BijectiveFunction<B, I> _bijectiveFunction;
	private final InverseFunction<I, B> _inverseFunction;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link InvertibleComposite} from the provided
	 * {@link BijectiveFunction} and the according {@link InverseFunction}.
	 * 
	 * @param aBijectiveFunction The {@link BijectiveFunction} responsible for
	 *        the {@link #applyBijection(Object)} operation.
	 * @param aInverseFunction The {@link InverseFunction} responsible for the
	 *        {@link #applyInversion(Object)} operation.
	 */
	public InvertibleComposite( BijectiveFunction<B, I> aBijectiveFunction, InverseFunction<I, B> aInverseFunction ) {
		_bijectiveFunction = aBijectiveFunction;
		_inverseFunction = aInverseFunction;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public B applyBijection( I aValue ) {
		return _bijectiveFunction.applyBijection( aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public I applyInversion( B aValue ) {
		return _inverseFunction.applyInversion( aValue );
	}
}
