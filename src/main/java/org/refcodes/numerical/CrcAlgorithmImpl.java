// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

// #############################################################################
// The following MIT License (MIT) is provided as of the code of this class has
// been copy'n'pasted from the Online CRC-8 CRC-16 CRC-32 Calculator
// (https://crccalc.com)'s GitHub (https://github.com/meetanthony/crcjava.git)
// repository:
// #############################################################################

// -----------------------------------------------------------------------------
// The MIT License (MIT)
//
// Copyright (c) 2017 Anton Isakov http://crccalc.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// -----------------------------------------------------------------------------

package org.refcodes.numerical;

/**
 * Implementation of the {@link CrcAlgorithm} interface. Credits to [Anton
 * Isakov (http://crccalc.com) of the "Online CRC-8 CRC-16 CRC-32 Calculator"
 * (https://crccalc.com)'s GitHub (https://github.com/meetanthony/crcjava.git)
 * repository.
 */
public class CrcAlgorithmImpl implements CrcAlgorithm {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * This is hash size. [Anton Isakov http://crccalc.com]
	 */
	final int _crcBitWidth;

	/**
	 * "This parameter specifies the initial value of the register when the
	 * algorithm starts.This is the value that is to be assigned to the register
	 * in the direct table algorithm. In the table algorithm, we may think of
	 * the register always commencing with the value zero, and this value being
	 * XORed into the register after the N'th bit iteration. This parameter
	 * should be specified as a hexadecimal number." [Anton Isakov
	 * http://crccalc.com]
	 */
	private final long _initCrc;

	/**
	 * "This is a name given to the algorithm. A string value." [Anton Isakov
	 * http://crccalc.com]
	 */
	private final String _name;

	/**
	 * "This parameter is the poly. This is a binary value that should be
	 * specified as a hexadecimal number.The top bit of the poly should be
	 * omitted.For example, if the poly is 10110, you should specify 06. An
	 * important aspect of this parameter is that it represents the unreflected
	 * poly; the bottom bit of this parameter is always the LSB of the divisor
	 * during the division regardless of whether the algorithm being modelled is
	 * reflected." [Anton Isakov http://crccalc.com]
	 */
	private final long _polynomial;

	/**
	 * "This is a boolean parameter. If it is FALSE, input bytes are processed
	 * with bit 7 being treated as the most significant bit (MSB) and bit 0
	 * being treated as the least significant bit.If this parameter is FALSE,
	 * each byte is reflected before being processed." [Anton Isakov
	 * http://crccalc.com]
	 */
	private final boolean _refIn;

	/**
	 * "This is a boolean parameter. If it is set to FALSE, the final value in
	 * the register is fed into the XOROUT stage directly, otherwise, if this
	 * parameter is TRUE, the final register value is reflected first." [Anton
	 * Isakov http://crccalc.com]
	 */
	private final boolean _refOut;

	/**
	 * "This is an W-bit value that should be specified as a hexadecimal
	 * number.It is XORed to the final register value (after the REFOUT) stage
	 * before the value is returned as the official checksum." [Anton Isakov
	 * http://crccalc.com]
	 */
	private final long _xorOut;

	private final long[] _table = new long[256];

	private long _mask = 0xFFFFFFFFFFFFFFFFL;

	private CrcSize _crcWidth = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link CrcAlgorithm} instance. For sound init arguments
	 * please use predefined {@link CrcAlgorithm} instances defined in the
	 * {@link CrcStandard} enumeration.
	 * 
	 * @param aName The descriptive name of the algorithm.
	 * @param aCrcBitWidth "This is hash size." [Anton Isakov
	 *        http://crccalc.com]
	 * @param aPolynomial "This parameter is the poly. This is a binary value
	 *        that should be specified as a hexadecimal number.The top bit of
	 *        the poly should be omitted.For example, if the poly is 10110, you
	 *        should specify 06. An important aspect of this parameter is that
	 *        it represents the unreflected poly; the bottom bit of this
	 *        parameter is always the LSB of the divisor during the division
	 *        regardless of whether the algorithm being modelled is reflected."
	 *        [Anton Isakov http://crccalc.com]
	 * @param aInitCrc "This parameter specifies the initial value of the
	 *        register when the algorithm starts.This is the value that is to be
	 *        assigned to the register in the direct table algorithm. In the
	 *        table algorithm, we may think of the register always commencing
	 *        with the value zero, and this value being XORed into the register
	 *        after the N'th bit iteration. This parameter should be specified
	 *        as a hexadecimal number." [Anton Isakov http://crccalc.com]
	 * @param aRefIn "This parameter is the poly. This is a binary value that
	 *        should be specified as a hexadecimal number.The top bit of the
	 *        poly should be omitted.For example, if the poly is 10110, you
	 *        should specify 06. An important aspect of this parameter is that
	 *        it represents the unreflected poly; the bottom bit of this
	 *        parameter is always the LSB of the divisor during the division
	 *        regardless of whether the algorithm being modelled is reflected."
	 *        [Anton Isakov http://crccalc.com]
	 * @param aRefOut "This is a boolean parameter. If it is set to FALSE, the
	 *        final value in the register is fed into the XOROUT stage directly,
	 *        otherwise, if this parameter is TRUE, the final register value is
	 *        reflected first." [Anton Isakov http://crccalc.com]
	 * @param aXorOut "This is an W-bit value that should be specified as a
	 *        hexadecimal number.It is XORed to the final register value (after
	 *        the REFOUT) stage before the value is returned as the official
	 *        checksum." [Anton Isakov http://crccalc.com]
	 */
	public CrcAlgorithmImpl( String aName, int aCrcBitWidth, long aPolynomial, long aInitCrc, boolean aRefIn, boolean aRefOut, long aXorOut ) {
		_name = aName;
		_initCrc = aInitCrc;
		_polynomial = aPolynomial;
		_refIn = aRefIn;
		_refOut = aRefOut;
		_xorOut = aXorOut;
		_crcBitWidth = aCrcBitWidth;
		if ( _crcBitWidth < 64 ) {
			_mask = ( 1L << _crcBitWidth ) - 1;
		}
		long eTmp;
		long eLastBit;
		for ( int i = 0; i < _table.length; i++ ) {
			eTmp = i;
			if ( _refIn ) {
				eTmp = NumericalUtility.toReversedLong( eTmp, _crcBitWidth );
			}
			else if ( _crcBitWidth > 8 ) {
				eTmp <<= ( _crcBitWidth - 8 );
			}
			eLastBit = ( 1L << ( _crcBitWidth - 1 ) );
			for ( int j = 0; j < 8; j++ ) {
				if ( ( eTmp & eLastBit ) != 0 ) {
					eTmp = ( ( eTmp << 1 ) ^ _polynomial );
				}
				else {
					eTmp <<= 1;
				}
			}
			if ( _refOut ) {
				eTmp = NumericalUtility.toReversedLong( eTmp, _crcBitWidth );
			}
			_table[i] = eTmp & _mask;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( byte aData ) {
		return toCrcChecksum( new byte[] { aData }, 0, 1 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( long aCrc, byte aData ) {
		return toCrcChecksum( aCrc, new byte[] { aData }, 0, 1 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( byte[] aData ) {
		return toCrcChecksum( aData, 0, aData.length );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( long aCrc, byte[] aData ) {
		return toCrcChecksum( aCrc, aData, 0, aData.length );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( byte[] aData, int aOffset, int aLength ) {
		final long theInitCrc = _refOut ? NumericalUtility.toReversedLong( _initCrc, _crcBitWidth ) : _initCrc;
		final long theChecksum = toCrcChecksum( theInitCrc, aData, aOffset, aLength );
		return ( theChecksum ^ _xorOut ) & _mask;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long toCrcChecksum( long aCrc, byte[] aData, int aOffset, int aLength ) {
		long theCrc = aCrc;
		if ( _refOut ) {
			for ( int i = aOffset; i < aOffset + aLength; i++ ) {
				theCrc = ( _table[(int) ( ( theCrc ^ aData[i] ) & 0xFF )] ^ ( theCrc >>> 8 ) );
				theCrc &= _mask;
			}
		}
		else {
			int toRight = ( _crcBitWidth - 8 );
			toRight = toRight < 0 ? 0 : toRight;
			for ( int i = aOffset; i < aOffset + aLength; i++ ) {
				theCrc = ( _table[(int) ( ( ( theCrc >> toRight ) ^ aData[i] ) & 0xFF )] ^ ( theCrc << 8 ) );
				theCrc &= _mask;
			}
		}
		return theCrc;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( byte aData, Endianess aEndianess ) {
		return toCrcBytes( new byte[] { aData }, 0, 1, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( long aCrc, byte aData, Endianess aEndianess ) {
		return toCrcBytes( aCrc, new byte[] { aData }, 0, 1, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( byte[] aData, Endianess aEndianess ) {
		return toCrcBytes( aData, 0, aData.length, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( long aCrc, byte[] aData, Endianess aEndianess ) {
		return toCrcBytes( aCrc, aData, 0, aData.length, aEndianess );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( byte[] aData, int aOffset, int aLength, Endianess aEndianess ) {
		final long theCrc = toCrcChecksum( aData, aOffset, aLength );
		return aEndianess.toUnsignedBytes( theCrc, _crcBitWidth / 8 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] toCrcBytes( long aCrc, byte[] aData, int aOffset, int aLength, Endianess aEndianess ) {
		final long theCrc = toCrcChecksum( aCrc, aData, aOffset, aLength );
		return aEndianess.toUnsignedBytes( theCrc, _crcBitWidth / 8 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCrcWidth() {
		return _crcBitWidth / 8;
	}

	/**
	 * Returns the {@link CrcSize} (describing the checksum's "CRC byte length"
	 * for this {@link CrcStandard}.
	 * 
	 * @return The according {@link CrcSize}.
	 */
	@Override
	public CrcSize getCrcSize() {
		if ( _crcWidth == null ) {
			synchronized ( this ) {
				if ( _crcWidth == null ) {
					_crcWidth = CrcSize.toCrcCategory( _crcBitWidth / 8 );
				}
			}
		}
		return _crcWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return _name;
	}
}
