// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

/**
 * Provides an accessor for a {@link Endianess} property.
 */
public interface EndianessAccessor {

	/**
	 * Retrieves the value from the {@link Endianess} property.
	 * 
	 * @return The name stored by the {@link Endianess} property.
	 */
	Endianess getEndianess();

	/**
	 * Provides a mutator for a {@link Endianess} property.
	 */
	public interface EndianessMutator {

		/**
		 * Sets the value for the {@link Endianess} property.
		 * 
		 * @param aEndianess The value to be stored by the {@link Endianess}
		 *        property.
		 */
		void setEndianess( Endianess aEndianess );
	}

	/**
	 * Provides a builder method for a {@link Endianess} property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface EndianessBuilder<B extends EndianessBuilder<B>> {

		/**
		 * Sets the value for the {@link Endianess} property.
		 * 
		 * @param aEndianess The value to be stored by the {@link Endianess}
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withEndianess( Endianess aEndianess );
	}

	/**
	 * Provides a {@link Endianess} property.
	 */
	public interface EndianessProperty extends EndianessAccessor, EndianessMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link Endianess}
		 * (setter) as of {@link #setEndianess(Endianess)} and returns the very
		 * same value (getter).
		 * 
		 * @param aEndianess The {@link Endianess} to set (via
		 *        {@link #setEndianess(Endianess)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Endianess letEndianess( Endianess aEndianess ) {
			setEndianess( aEndianess );
			return aEndianess;
		}
	}
}
