// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.refcodes.data.BooleanLiterals;
import org.refcodes.data.CharSet;
import org.refcodes.data.Encoding;

/**
 * This class contains some useful static methods for working with bitwise
 * operations objects.
 * <p>
 * CATION: This class may not have an optimized runtime behavior!
 */
public final class NumericalUtility {

	private static final int BYTE_MASK = 0xFF;
	private static final int BYTES_PER_INT = 4;
	private static final int BYTES_PER_LONG = 8;
	private static final long BITS_PER_BYTE = 8;
	private static final BigInteger UNSIGNED_LONG_MASK = BigInteger.ONE.shiftLeft( Long.SIZE ).subtract( BigInteger.ONE );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private NumericalUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// NUMBER BASE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Converts a value to a {@link String} of digits of the provided number
	 * base.
	 * 
	 * @param aValue The value to be converted.
	 * @param aToBase The number base to which to convert.
	 * 
	 * @return The according digits' {@link String}.
	 */
	public static String toNumberBase( long aValue, int aToBase ) {
		final int[] theDigits = toDigits( aValue, aToBase );
		final StringBuilder theBuilder = new StringBuilder();
		for ( int eDigit : theDigits ) {
			theBuilder.append( CharSet.ARABIC_BASE64.getCharSet()[eDigit] );
		}
		return theBuilder.toString();
	}

	/**
	 * Converts a digits {@link String} of the provided number base to a value.
	 * 
	 * @param aFromBase The number base from which to convert.
	 * @param aDigits The digits' {@link String} to be converted back to a
	 *        value.
	 * 
	 * @return The according value.
	 */
	public static long fromNumberBase( int aFromBase, String aDigits ) {

		final int[] theDigits = new int[aDigits.length()];
		for ( int i = 0; i < theDigits.length; i++ ) {
			theDigits[i] = toIndex( aDigits.charAt( i ), CharSet.ARABIC_BASE64.getCharSet() );

		}
		return fromDigits( aFromBase, theDigits );
	}

	/**
	 * Converts a digits' {@link String} of the provided "from" number base to a
	 * digits' {@link String} of the provided "to" number base.
	 * 
	 * @param aFromBase The number base from which to convert.
	 * @param aDigits The digits' {@link String} to be converted.
	 * @param aToBase The number base to which to convert.
	 * 
	 * @return The accordingly converted digits' {@link String}.
	 */
	public static String convertNumberBase( int aFromBase, String aDigits, int aToBase ) {
		return toNumberBase( fromNumberBase( aFromBase, CharSet.ARABIC_BASE64.getCharSet(), aDigits ), aToBase, CharSet.ARABIC_BASE64.getCharSet() );
	}

	/**
	 * Converts a value to a {@link String} of digits of the provided number
	 * base using the char set as digit representatives.
	 * 
	 * @param aValue The value to be converted.
	 * @param aToBase The number base to which to convert.
	 * @param aToCharSet The char set representing the digits.
	 * 
	 * @return The according digits' {@link String}.
	 */
	public static String toNumberBase( long aValue, int aToBase, char[] aToCharSet ) {
		final int[] theDigits = toDigits( aValue, aToBase );
		final StringBuilder theBuilder = new StringBuilder();
		for ( int eDigit : theDigits ) {
			theBuilder.append( aToCharSet[eDigit] );
		}
		return theBuilder.toString();
	}

	/**
	 * Converts a digits {@link String} of the provided number base to a value
	 * using the char set as digit representatives.
	 * 
	 * @param aFromBase The number base from which to convert.
	 * @param aFromCharSet The char set representing the digits.
	 * @param aDigits The digits' {@link String} to be converted back to a
	 *        value.
	 * 
	 * @return The according value.
	 */
	public static long fromNumberBase( int aFromBase, char[] aFromCharSet, String aDigits ) {

		final int[] theDigits = new int[aDigits.length()];
		for ( int i = 0; i < theDigits.length; i++ ) {
			theDigits[i] = toIndex( aDigits.charAt( i ), aFromCharSet );

		}
		return fromDigits( aFromBase, theDigits );
	}

	/**
	 * Converts a digits' {@link String} of the provided "from" number base to a
	 * digits' {@link String} of the provided "to" number base using the "from"
	 * and "to" char sets as digit representatives.
	 * 
	 * @param aFromBase The number base from which to convert.
	 * @param aFromCharSet The char set representing the digits.
	 * @param aDigits The digits' {@link String} to be converted.
	 * @param aToBase The number base to which to convert.
	 * @param aToCharSet The char set representing the digits.
	 * 
	 * @return The accordingly converted digits' {@link String}.
	 */
	public static String convertNumberBase( int aFromBase, char[] aFromCharSet, String aDigits, int aToBase, char[] aToCharSet ) {
		return toNumberBase( fromNumberBase( aFromBase, aFromCharSet, aDigits ), aToBase, aToCharSet );
	}

	/**
	 * Converts a value to an array of digits of the provided number base.
	 * 
	 * @param aValue The value to be converted.
	 * @param aToBase The number base to which to convert.
	 * 
	 * @return The according digits.
	 */
	public static int[] toDigits( long aValue, int aToBase ) {
		final List<Integer> theDigits = new ArrayList<>();
		while ( aValue > 0 ) {
			theDigits.add( 0, (int) ( aValue % aToBase ) );
			aValue /= aToBase;
		}
		return toPrimitiveArray( theDigits );
	}

	/**
	 * Converts an array of digits of the provided number base to a value.
	 * 
	 * @param aFromBase The number base from which to convert.
	 * @param aDigits The digits to be converted back to a value.
	 * 
	 * @return The according value.
	 */
	public static long fromDigits( int aFromBase, int[] aDigits ) {
		long theNumber = 0;
		for ( int eDigit : aDigits ) {
			theNumber = aFromBase * theNumber + eDigit;
		}
		return theNumber;
	}

	/**
	 * Converts an array of digits of the provided "from" number base to an
	 * array of digits of the provided "to" number base.
	 * 
	 * @param aFromBase The number base from which to convert.
	 * @param aDigits The digits to be converted.
	 * @param aToBase The number base to which to convert.
	 * 
	 * @return The accordingly converted digits.
	 */
	public static int[] convertDigits( int aFromBase, int[] aDigits, int aToBase ) {
		return toDigits( fromDigits( aFromBase, aDigits ), aToBase );
	}

	private static int[] toPrimitiveArray( List<Integer> aValues ) {
		final int[] theResult = new int[aValues.size()];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = aValues.get( i );
		}
		return theResult;
	}

	private static int toIndex( char aChar, char[] aCharSet ) {
		for ( int i = 0; i < aCharSet.length; i++ ) {
			if ( aCharSet[i] == aChar ) {
				return i;
			}
		}
		throw new IllegalArgumentException( "The character <" + aChar + "> is not contained withon the char set <" + Arrays.toString( aCharSet ) + ">!" );
	}

	// /////////////////////////////////////////////////////////////////////////
	// BOOLEAN:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines the whether a bit at a given position of the provided is set.
	 * 
	 * @param aValue The value which's bits to test.
	 * 
	 * @param aPosition The position of the bit in question.
	 * 
	 * @return True in case it is set, else false,
	 */
	public static boolean isBitSetAt( long aValue, int aPosition ) {
		return ( ( aValue >> aPosition ) & 1 ) == 1;
	}

	/**
	 * Sets the bit of the given value at the given bit position as specified.
	 * 
	 * @param aValue The value where to set the bit.
	 * @param aPos The bit of the value which to set accordingly.
	 * @param aBit The value of the bit.
	 * 
	 * @return The byte with the according bit set accordingly.
	 */
	public static byte setBitAt( byte aValue, int aPos, boolean aBit ) {
		return (byte) ( ( aBit ) ? ( aValue | ( 1 << aPos ) ) : ( aValue & ~( 1 << aPos ) ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONVERSION:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Converts a {@link String} containing "verbose" bytes into an array of
	 * bytes. The {@link String} might look as follows: "100,12,0x14, 0xFF".
	 * Hexadecimal values must(!) be prefixed with "X0" or "x0"!
	 * 
	 * @param aString The String which is to be converted into bytes.
	 * 
	 * @return The according byte array.
	 * 
	 * @throws IllegalArgumentException in case the provided {@link String}
	 *         contains illegal characters.
	 * 
	 * @throws NumberFormatException in case the String contains values which
	 *         cannot be parsed.
	 */
	public static byte[] toBytes( String aString ) {
		return toBytes( aString, false );
	}

	/**
	 * Converts a {@link String} containing "verbose" bytes into an array of
	 * bytes. The {@link String} might look as follows: "100,12,0x14, 0xFF"
	 * 
	 * @param aString The String which is to be converted into bytes.
	 * @param isHexString True in case you provide a hexadecimal {@link String}
	 *        which may not have "0X" prefixed to its values. E.g. each value is
	 *        treated as being hexadecimal.
	 * 
	 * @return The according byte array.
	 * 
	 * @throws IllegalArgumentException in case the provided {@link String}
	 *         contains illegal characters.
	 * 
	 * @throws NumberFormatException in case the String contains values which
	 *         cannot be parsed.
	 */
	public static byte[] toBytes( String aString, boolean isHexString ) {
		final List<Byte> theBytes = new ArrayList<>();
		final StringBuilder theNormalized = new StringBuilder();
		char eChar;
		for ( int i = 0; i < aString.length(); i++ ) {
			eChar = aString.charAt( i );
			if ( eChar == 'x' || eChar == 'X' || Character.isDigit( eChar ) || ( eChar >= 'a' ) && ( eChar <= 'f' ) || ( eChar >= 'A' ) && ( eChar <= 'F' ) || eChar == '-' ) {
				theNormalized.append( eChar );
			}
			else if ( eChar == ',' || eChar == ' ' ) {
				theNormalized.append( ',' );
			}
			else if ( eChar != '{' && eChar != '}' && eChar != '[' && eChar != ']' && eChar != '(' && eChar != ')' ) {
				throw new IllegalArgumentException( "Invalid character '" + eChar + "' at index <" + i + " of String \"" + aString + "\"!" );
			}
		}
		final StringTokenizer theTokenizer = new StringTokenizer( theNormalized.toString(), ",", false );
		String eToken;
		int eByte;
		while ( theTokenizer.hasMoreTokens() ) {
			eToken = theTokenizer.nextToken();
			try {
				if ( isHexString || eToken.startsWith( "0x" ) || eToken.startsWith( "0X" ) ) {
					if ( eToken.startsWith( "0x" ) || eToken.startsWith( "0X" ) ) {
						eToken = eToken.substring( 2 );
					}
					eByte = Integer.valueOf( eToken, 16 );
				}
				else {
					eByte = Integer.valueOf( eToken );
				}
			}
			catch ( NumberFormatException e ) {
				throw new NumberFormatException( "Value \"" + eToken + "\" of string \"" + aString + "\" cannot be parsed: " + e.getMessage() );
			}

			if ( eByte <= 255 && eByte >= Byte.MIN_VALUE ) {
				theBytes.add( (byte) eByte );
			}
			else {
				throw new NumberFormatException( "The value <" + eByte + "> (\"" + eToken + "\") of text \"" + aString + "\" is out of range (0-255) !" );
			}
		}
		final byte[] theResult = new byte[theBytes.size()];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = theBytes.get( i );
		}
		return theResult;
	}

	/**
	 * Converts a long value to an array of bytes, left byte being the most
	 * significant one (highest order).
	 * 
	 * @param aLong The long value to be converted
	 * 
	 * @return The array of bytes representing the long value.
	 */
	public static byte[] toBytes( long aLong ) {
		final byte[] theBytes = new byte[BYTES_PER_LONG];
		for ( int i = 0; i < BYTES_PER_LONG; i++ ) {
			theBytes[BYTES_PER_LONG - 1 - i] = (byte) ( aLong >>> ( i * BITS_PER_BYTE ) );
		}
		return theBytes;
	}

	/**
	 * Converts a integer value to an array of bytes, left byte being the most
	 * significant one (highest order).
	 * 
	 * @param aInteger The integer value to be converted
	 * 
	 * @return The array of bytes representing the long value.
	 */
	public static byte[] toBytes( int aInteger ) {
		final byte[] theBytes = new byte[BYTES_PER_INT];
		for ( int i = 0; i < BYTES_PER_INT; i++ ) {
			theBytes[BYTES_PER_INT - 1 - i] = (byte) ( aInteger >>> ( i * BITS_PER_BYTE ) );
		}
		return theBytes;
	}

	/**
	 * Converts a boolean array to a byte array using the byte's bits to
	 * represent the according boolean values. Attention: Trailing bits of the
	 * last byte might need to be ignored in case the length of the boolean
	 * array is not a multiple of 8.
	 * 
	 * @param aBits The boolean array to convert.
	 * 
	 * @return The byte array which's bits represent the according boolean
	 *         array's values.
	 */
	public static byte[] toBytes( boolean[] aBits ) {
		final byte[] theBytes = new byte[aBits.length / 8 + ( aBits.length % 8 == 0 ? 0 : 1 )];
		int index;
		for ( int i = 0; i < theBytes.length; i++ ) {
			for ( int j = 0; j < 8; j++ ) {
				index = i * 8 + j;
				if ( index >= aBits.length ) {
					break;
				}
				if ( aBits[index] ) {
					theBytes[i] |= ( 128 >> j );
				}
			}
		}
		return theBytes;
	}

	/**
	 * Converts the bits contained within the given byte array to a boolean
	 * array.
	 * 
	 * @param aBytes The bytes to be converted.
	 * 
	 * @return The according boolean array.
	 */
	public static boolean[] toBooleans( byte[] aBytes ) {
		return toBooleans( aBytes, 0, aBytes.length * 8 );
	}

	/**
	 * Converts the bits contained within the given byte array to a boolean
	 * array.
	 * 
	 * @param aBytes The bytes to be converted.
	 * @param aLength The length of the resulting boolean array.
	 * 
	 * @return The according boolean array.
	 */
	public static boolean[] toBooleans( byte[] aBytes, int aLength ) {
		return toBooleans( aBytes, 0, aLength );
	}

	/**
	 * Converts the bits contained within the given byte array to a boolean
	 * array.
	 * 
	 * @param aBytes The bytes to be converted.
	 * @param aOffset The offset of the bytes from where to start converting.
	 * @param aLength The length of the resulting boolean array.
	 * 
	 * @return The according boolean array.
	 */
	public static boolean[] toBooleans( byte[] aBytes, int aOffset, int aLength ) {
		final boolean[] theBooleans = new boolean[aLength];
		int index;
		for ( int i = 0; i < aLength; i++ ) {
			for ( int j = 0; j < 8; j++ ) {
				index = i * 8 + j;
				if ( index >= theBooleans.length || i + aOffset >= aBytes.length ) {
					break;
				}
				theBooleans[index] = ( aBytes[i + aOffset] & (byte) ( 128 / Math.pow( 2, j ) ) ) != 0;
			}
		}
		return theBooleans;
	}

	/**
	 * Converts a long value from the given byte array.
	 *
	 * @param aBytes The bytes to convert.
	 * 
	 * @return The resulting long value.
	 */
	public static long toLong( byte[] aBytes ) {
		return toLong( aBytes, 0 );
	}

	/**
	 * Converts a long value from the given byte array.
	 *
	 * @param aBytes The bytes to convert.
	 * @param aOffset The offset from where to start conversion.
	 * 
	 * @return The resulting long value.
	 * 
	 * @throws IllegalArgumentException the illegal argument exception
	 */
	public static long toLong( byte[] aBytes, int aOffset ) {
		long theWord = 0;
		for ( int i = 0; i < BYTES_PER_LONG && i + aOffset < aBytes.length; i++ ) {
			theWord <<= BITS_PER_BYTE;
			if ( i < aBytes.length ) {
				theWord |= (long) aBytes[i + aOffset] & BYTE_MASK;
			}
		}
		return theWord;
	}

	/**
	 * Converts an integer value from the given byte array.
	 *
	 * @param aBytes The bytes to convert.
	 * 
	 * @return The resulting integer value.
	 */
	public static int toInt( byte[] aBytes ) {
		return toInt( aBytes, 0 );
	}

	/**
	 * Converts an integer value from the given byte array.
	 *
	 * @param aBytes The bytes to convert.
	 * @param aOffset The offset from where to start conversion.
	 * 
	 * @return The resulting integer value.
	 */
	public static int toInt( byte[] aBytes, int aOffset ) {
		int theWord = 0;
		for ( int i = 0; i < BYTES_PER_INT && i + aOffset < aBytes.length; i++ ) {
			theWord <<= BITS_PER_BYTE;
			if ( i < aBytes.length ) {
				theWord |= aBytes[i + aOffset] & BYTE_MASK;
			}
		}
		return theWord;
	}

	/**
	 * Converts a byte array to a short array.
	 * 
	 * @param aBytes The bytes to be converted to short.
	 * 
	 * @return The according short array.
	 */
	public static short[] toShorts( byte[] aBytes ) {
		if ( aBytes != null ) {
			final short[] theShorts = new short[aBytes.length];
			for ( int i = 0; i < aBytes.length; i++ ) {
				theShorts[i] = aBytes[i];
			}
			return theShorts;
		}
		return null;
	}

	/**
	 * Converts a double value to an array of bytes, left(?) byte being the most
	 * significant one (highest order).
	 * 
	 * @param aDouble The double value to be converted
	 * 
	 * @return The array of bytes representing the double value.
	 */
	public static byte[] toBytes( Double aDouble ) {
		if ( aDouble != null ) {
			final byte[] theBytes = new byte[Double.BYTES];
			final long theLong = Double.doubleToLongBits( aDouble );
			for ( int i = 0; i < 8; i++ ) {
				theBytes[i] = (byte) ( ( theLong >> ( ( 7 - i ) * 8 ) ) & 0xff );
			}
			return theBytes;
		}
		return null;
	}

	/**
	 * Creates a long from a {@link String}; useful e.g. when creating a number
	 * from a pass-phrase.
	 * 
	 * @param aString The {@link String} to be converted to a long value.
	 * 
	 * @return The long value from the {@link String}
	 */
	public static long toLong( String aString ) {
		byte[] theBytes;
		try {
			theBytes = aString.getBytes( Encoding.UTF_8.getCode() );
		}
		catch ( UnsupportedEncodingException e ) {
			theBytes = aString.getBytes();
		}
		long theValue;
		// if ( theBytes.length < 8 ) {
		theValue = 0;
		for ( byte theByte : theBytes ) {
			theValue = ( theValue << 8 ) + ( theByte & 0xff );
		}
		return theValue;
	}

	/**
	 * Creates a double between 0 and 1 from a {@link String}; useful e.g. when
	 * creating a number from a pass-phrase.
	 * 
	 * @param aString The {@link String} to be converted to a double value.
	 * 
	 * @return The double value between 0 and 1 from the {@link String}.
	 */
	public static double toDouble( String aString ) {
		final long theLong = toLong( aString );
		final double theDouble;
		if ( theLong >= 0 ) {
			theDouble = ( (double) theLong ) / ( (double) Long.MAX_VALUE );
		}
		else {
			theDouble = 1 - ( (double) theLong ) / ( (double) Long.MIN_VALUE );
		}
		return theDouble;
	}

	/**
	 * Creates an array with double values between 0 and 1 from a {@link String}
	 * useful e.g. when creating a bunch of numbers from a pass-phrase.
	 * 
	 * @param aString The {@link String} to be converted to a double value.
	 * @param aCount The number of doubles to create.
	 * 
	 * @return The array with double values between 0 and 1 from the
	 *         {@link String}.
	 */
	public static double[] toDoubles( String aString, int aCount ) {
		if ( aCount <= 0 ) {
			throw new IllegalArgumentException( "The provided count <" + aCount + "> must be greatet than one!" );
		}
		final double[] theResult = new double[aCount];
		final String[] theStrings = new String[aCount];
		int step = aString.length() / aCount;
		if ( step == 0 ) {
			step = 1;
		}
		int index;
		for ( int i = 0; i < aCount; i++ ) {
			theStrings[i] = "";
			index = step * i;
			for ( int j = 0; j < aString.length(); j++ ) {
				if ( index >= aString.length() ) {
					index = 0;
				}
				theStrings[i] = theStrings[i] + aString.charAt( index );
				index++;
			}
			theResult[i] = toDouble( theStrings[i] );
			// System.out.println( theStrings[i] );
		}
		return theResult;
	}

	/**
	 * Calculates a hash code from the given object's hash codes.
	 * 
	 * @param aObjects The objects for which to calculate the has codes.
	 * 
	 * @return The hash codes for the given objects.
	 */
	public static int toHashCode( Object... aObjects ) {
		final int thePrime = 31;
		int theResult = 1;
		for ( Object eObject : aObjects ) {
			theResult = thePrime * theResult + ( ( eObject == null ) ? 0 : eObject.hashCode() );
		}
		return theResult;
	}

	/**
	 * Converts a signed int to an unsigned long value. This is not do a simple
	 * ABS function, here we take a look at the bits of the value and calculate
	 * which the unsigned (positive) value would be. The positive value is
	 * returned in a (signed) long value.
	 * 
	 * @param aSignedIntValue The signed integer value.
	 * 
	 * @return The unsigned long value.
	 */
	public static long toUnsignedLong( int aSignedIntValue ) {
		if ( aSignedIntValue >= 0 ) {
			return aSignedIntValue;
		}
		final long theSignedValue = aSignedIntValue;
		return theSignedValue & 0x00000000ffffffffL;
	}

	// -------------------------------------------------------------------------
	// LITTLE ENDIAN:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given value to a byte array in little endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * @param aLength The number of bytes to use.
	 * 
	 * @return The according array.
	 */
	public static byte[] toLittleEndianBytes( long aValue, int aLength ) {
		final byte[] theResult = toBigEndianBytes( aValue, aLength );
		byte eTmp;
		for ( int i = 0; i < theResult.length / 2; i++ ) {
			eTmp = theResult[i];
			theResult[i] = theResult[theResult.length - i - 1];
			theResult[theResult.length - i - 1] = eTmp;
		}
		return theResult;
	}

	// -------------------------------------------------------------------------
	// LONG:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given value to a byte array in little endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toLittleEndianBytes( long aValue ) {
		return toLittleEndianBytes( aValue, Long.BYTES );
	}

	// -------------------------------------------------------------------------
	// INT:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given value to a byte array in little endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toLittleEndianBytes( int aValue ) {
		return toLittleEndianBytes( aValue, Integer.BYTES );
	}

	// -------------------------------------------------------------------------
	// SHORT:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given value to a byte array in little endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toLittleEndianBytes( short aValue ) {
		return toLittleEndianBytes( aValue, Short.BYTES );
	}

	// -------------------------------------------------------------------------
	// BIG ENDIAN:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given value to a byte array in big endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * @param aLength The number of bytes to use.
	 * 
	 * @return The according array,
	 */
	public static byte[] toBigEndianBytes( long aValue, int aLength ) {
		final BigInteger theBigInt = BigInteger.valueOf( aValue );
		final byte[] theBytes = theBigInt.toByteArray();
		// Java is Big-Endian |-->
		if ( theBytes.length == aLength ) {
			return theBytes;
		}
		// Java is Big-Endian <--|

		final byte[] theResult = new byte[aLength];

		for ( int i = 0; i < theResult.length; i++ ) {
			if ( theBytes.length > i ) {
				theResult[theResult.length - i - 1] = theBytes[theBytes.length - i - 1];
			}
			else {
				theResult[theResult.length - i - 1] = aValue < 0 ? (byte) 0xFF : 0;
			}
		}
		return theResult;
	}

	// -------------------------------------------------------------------------
	// LONG:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given value to a byte array in big endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toBigEndianBytes( long aValue ) {
		return toBigEndianBytes( aValue, Long.BYTES );
	}

	// -------------------------------------------------------------------------
	// INT:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given value to a byte array in big endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toBigEndianBytes( int aValue ) {
		return toBigEndianBytes( aValue, Integer.BYTES );
	}

	// -------------------------------------------------------------------------
	// SHORT:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given value to a byte array in big endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toBigEndianBytes( short aValue ) {
		return toBigEndianBytes( aValue, Short.BYTES );
	}

	// -------------------------------------------------------------------------
	// UNSIGEND LITTLE ENDIAN:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) value to a byte array in little endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * @param aLength The number of bytes to use.
	 * 
	 * @return The according array.
	 */
	public static byte[] toUnsignedLittleEndianBytes( long aValue, int aLength ) {
		final byte[] theResult = new byte[aLength];
		final BigInteger theBigInt = BigInteger.valueOf( aValue ).and( UNSIGNED_LONG_MASK );
		final byte[] theBytes = theBigInt.toByteArray();
		for ( int i = 0; i < theResult.length; i++ ) {
			if ( theBytes.length > i ) {
				theResult[i] = theBytes[theBytes.length - 1 - i];
			}
			else {
				theResult[i] = 0;
			}
		}
		return theResult;
	}

	// -------------------------------------------------------------------------
	// LONG:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) value to a byte array in little endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toUnsignedLittleEndianBytes( long aValue ) {
		return toUnsignedLittleEndianBytes( aValue, Long.BYTES );
	}

	// -------------------------------------------------------------------------
	// INT:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) value to a byte array in little endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toUnsignedLittleEndianBytes( int aValue ) {
		return toUnsignedLittleEndianBytes( aValue, Integer.BYTES );
	}

	// -------------------------------------------------------------------------
	// SHORT:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) value to a byte array in little endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toUnsignedLittleEndianBytes( short aValue ) {
		return toUnsignedLittleEndianBytes( aValue, Short.BYTES );
	}

	// -------------------------------------------------------------------------
	// UNSIGNED BIG ENDIAN:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) value to a byte array in big endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * @param aLength The number of bytes to use.
	 * 
	 * @return The according array.
	 */
	public static byte[] toUnsignedBigEndianBytes( long aValue, int aLength ) {
		final byte[] theResult = new byte[aLength];
		final BigInteger theBigInt = BigInteger.valueOf( aValue ).and( UNSIGNED_LONG_MASK );
		final byte[] theBytes = theBigInt.toByteArray();
		for ( int i = 0; i < theResult.length; i++ ) {
			if ( theBytes.length > i ) {
				theResult[theResult.length - i - 1] = theBytes[theBytes.length - i - 1];
			}
			else {
				theResult[theResult.length - i - 1] = 0;
			}
		}
		return theResult;
	}

	// -------------------------------------------------------------------------
	// LONG:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) value to a byte array in big endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toUnsignedBigEndianBytes( long aValue ) {
		return toUnsignedBigEndianBytes( aValue, Long.BYTES );
	}

	// -------------------------------------------------------------------------
	// INT:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) value to a byte array in big endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toUnsignedBigEndianBytes( int aValue ) {
		return toUnsignedBigEndianBytes( aValue, Integer.BYTES );
	}

	// -------------------------------------------------------------------------
	// SHORT:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) value to a byte array in big endian notation.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toUnsignedBigEndianBytes( short aValue ) {
		return toUnsignedBigEndianBytes( aValue, Short.BYTES );
	}

	// -------------------------------------------------------------------------
	// LONG:
	// -------------------------------------------------------------------------

	/**
	 * Converts a byte array in little endian to a long value.
	 * 
	 * @param aLittleEndian The byte array to be converted.
	 * 
	 * @return The resulting long value.
	 */
	public static long toLongFromLittleEndianBytes( byte[] aLittleEndian ) {
		final byte[] theBigEndian = new byte[aLittleEndian.length];
		for ( int i = 0; i < aLittleEndian.length; i++ ) {
			theBigEndian[i] = aLittleEndian[aLittleEndian.length - i - 1];
		}
		final BigInteger theBigInt = new BigInteger( theBigEndian );
		return theBigInt.longValue();
	}

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) byte array in little endian to a long value.
	 * 
	 * @param aLittleEndian The byte array to be converted.
	 * 
	 * @return The resulting long value.
	 */
	public static long toUnsignedLongFromLittleEndianBytes( byte[] aLittleEndian ) {
		return toLongFromLittleEndianBytes( toUnsignedLittleEndianBytes( aLittleEndian ) );
	}

	/**
	 * Converts a byte array in big endian to a long value.
	 * 
	 * @param aBigEndian The byte array to be converted.
	 * 
	 * @return The resulting long value.
	 */
	public static long toLongFromBigEndianBytes( byte[] aBigEndian ) {
		final BigInteger theBigInt = new BigInteger( aBigEndian );
		return theBigInt.longValue();
	}

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned)a byte array in big endian to a long value.
	 * 
	 * @param aBigEndian The byte array to be converted.
	 * 
	 * @return The resulting long value.
	 */
	public static long toUnsignedLongFromBigEndianBytes( byte[] aBigEndian ) {
		return toLongFromBigEndianBytes( toUnsignedBigEndianBytes( aBigEndian ) );
	}

	// -------------------------------------------------------------------------
	// INT:
	// -------------------------------------------------------------------------

	/**
	 * Converts a byte array in little endian to an integer value.
	 * 
	 * @param aLittleEndian The byte array to be converted.
	 * 
	 * @return The resulting integer value.
	 */
	public static int toIntFromLittleEndianBytes( byte[] aLittleEndian ) {
		final byte[] theBigEndian = new byte[aLittleEndian.length];
		for ( int i = 0; i < aLittleEndian.length; i++ ) {
			theBigEndian[i] = aLittleEndian[aLittleEndian.length - i - 1];
		}
		final BigInteger theBigInt = new BigInteger( theBigEndian );
		return theBigInt.intValue();
	}

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) byte array in big endian to a long value.
	 * 
	 * @param aLittleEndian The byte array to be converted.
	 * 
	 * @return The resulting long value.
	 */
	public static int toUnsignedIntFromLittleEndianBytes( byte[] aLittleEndian ) {
		return toIntFromLittleEndianBytes( toUnsignedLittleEndianBytes( aLittleEndian ) );
	}

	/**
	 * Converts a byte array in big endian to an integer value.
	 * 
	 * @param aBigEndian The byte array to be converted.
	 * 
	 * @return The resulting integer value.
	 */
	public static int toIntFromBigEndianBytes( byte[] aBigEndian ) {
		final BigInteger theBigInt = new BigInteger( aBigEndian );
		return theBigInt.intValue();
	}

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) byte array in big endian to a long value.
	 * 
	 * @param aBigEndian The byte array to be converted.
	 * 
	 * @return The resulting long value.
	 */
	public static int toUnsignedIntFromBigEndianBytes( byte[] aBigEndian ) {
		return toIntFromBigEndianBytes( toUnsignedBigEndianBytes( aBigEndian ) );
	}

	// -------------------------------------------------------------------------
	// SHORT:
	// -------------------------------------------------------------------------

	/**
	 * Converts a byte array in little endian to an short value.
	 * 
	 * @param aLittleEndian The byte array to be converted.
	 * 
	 * @return The resulting short value.
	 */
	public static short toShortFromLittleEndianBytes( byte[] aLittleEndian ) {
		final byte[] theBigEndian = new byte[aLittleEndian.length];
		for ( short i = 0; i < aLittleEndian.length; i++ ) {
			theBigEndian[i] = aLittleEndian[aLittleEndian.length - i - 1];
		}
		final BigInteger theBigInt = new BigInteger( theBigEndian );
		return theBigInt.shortValue();
	}

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) byte array in big endian to a long value.
	 * 
	 * @param aLittleEndian The byte array to be converted.
	 * 
	 * @return The resulting long value.
	 */
	public static short toUnsignedShortFromLittleEndianBytes( byte[] aLittleEndian ) {
		return toShortFromLittleEndianBytes( toUnsignedLittleEndianBytes( aLittleEndian ) );
	}

	/**
	 * Converts a byte array in big endian to an short value.
	 * 
	 * @param aBigEndian The byte array to be converted.
	 * 
	 * @return The resulting short value.
	 */
	public static short toShortFromBigEndianBytes( byte[] aBigEndian ) {
		final BigInteger theBigInt = new BigInteger( aBigEndian );
		return theBigInt.shortValue();
	}

	/**
	 * Converts the given "unsigned" (it is to be treated as if it were
	 * unsigned) byte array in big endian to a long value.
	 * 
	 * @param aBigEndian The byte array to be converted.
	 * 
	 * @return The resulting long value.
	 */
	public static short toUnsignedShortFromBigEndianBytes( byte[] aBigEndian ) {
		return toShortFromBigEndianBytes( toUnsignedBigEndianBytes( aBigEndian ) );
	}

	// -------------------------------------------------------------------------

	/**
	 * Converts the given byte array to a byte array representing an unsigned
	 * value.
	 * 
	 * @param aLittleEndian The byte array to be converted to be unsigned.
	 * 
	 * @return The unsigned byte array, usually one byte longer.
	 */
	public static byte[] toUnsignedLittleEndianBytes( byte[] aLittleEndian ) {

		if ( aLittleEndian[aLittleEndian.length - 1] == 0 ) {
			return aLittleEndian;
		}

		final byte[] theLittleEndian = new byte[aLittleEndian.length + 1];
		for ( int i = 0; i < aLittleEndian.length; i++ ) {
			theLittleEndian[i] = aLittleEndian[i];
		}
		theLittleEndian[theLittleEndian.length - 1] = 0;
		return theLittleEndian;
	}

	/**
	 * Converts the given byte array to a byte array representing an unsigned
	 * value.
	 * 
	 * @param aBigEndian The byte array to be converted to be unsigned.
	 * 
	 * @return The unsigned byte array, usually one byte longer.
	 */
	public static byte[] toUnsignedBigEndianBytes( byte[] aBigEndian ) {

		if ( aBigEndian[0] == 0 ) {
			return aBigEndian;
		}

		final byte[] theBigEndian = new byte[aBigEndian.length + 1];
		for ( int i = 0; i < aBigEndian.length; i++ ) {
			theBigEndian[i + 1] = aBigEndian[i];
		}
		theBigEndian[0] = 0;
		return theBigEndian;
	}

	// -------------------------------------------------------------------------
	// FLOAT:
	// -------------------------------------------------------------------------

	/**
	 * Converts a byte array in big endian to a float value.
	 * 
	 * @param aBigEndian The byte array to be converted.
	 * 
	 * @return The resulting float value.
	 */
	public static float toFloatFromBigEndianBytes( byte[] aBigEndian ) {
		return ByteBuffer.wrap( aBigEndian ).getFloat();
	}

	/**
	 * Converts a byte array in big little to a float value.
	 * 
	 * @param aLittleEndian The byte array to be converted.
	 * 
	 * @return The resulting float value.
	 */
	public static float toFloatFromLittleEndianBytes( byte[] aLittleEndian ) {
		final byte[] theBigEndian = new byte[aLittleEndian.length];
		for ( int i = 0; i < aLittleEndian.length; i++ ) {
			theBigEndian[i] = aLittleEndian[aLittleEndian.length - i - 1];
		}
		return ByteBuffer.wrap( theBigEndian ).getFloat();
	}

	/**
	 * Converts the given value to a byte array in big endian notation. The
	 * length for a long are 32 bits, so the result will be 4 bytes long.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toBigEndianBytes( float aValue ) {
		final byte[] theBytes = new byte[Float.BYTES];
		ByteBuffer.wrap( theBytes ).putFloat( aValue );
		return theBytes;
	}

	/**
	 * Converts the given value to a byte array in big endian notation. The
	 * length for a long are 32 bits, so the result will be 4 bytes long.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toLittleEndianBytes( float aValue ) {
		final byte[] theBytes = new byte[4];
		ByteBuffer.wrap( theBytes ).putFloat( aValue );
		final byte[] theResult = new byte[theBytes.length];
		for ( int i = 0; i < theResult.length; i++ ) {
			if ( theBytes.length > i ) {
				theResult[i] = theBytes[theBytes.length - 1 - i];
			}
			else {
				theResult[i] = 0;
			}
		}
		return theResult;
	}

	// -------------------------------------------------------------------------
	// DOUBLE:
	// -------------------------------------------------------------------------

	/**
	 * Converts a byte array in big endian to a double value.
	 * 
	 * @param aBigEndian The byte array to be converted.
	 * 
	 * @return The resulting double value.
	 */
	public static double toDoubleFromBigEndianBytes( byte[] aBigEndian ) {
		return ByteBuffer.wrap( aBigEndian ).getDouble();
	}

	/**
	 * Converts a byte array in big little to a double value.
	 * 
	 * @param aLittleEndian The byte array to be converted.
	 * 
	 * @return The resulting double value.
	 */
	public static double toDoubleFromLittleEndianBytes( byte[] aLittleEndian ) {
		final byte[] theBigEndian = new byte[aLittleEndian.length];
		for ( int i = 0; i < aLittleEndian.length; i++ ) {
			theBigEndian[i] = aLittleEndian[aLittleEndian.length - i - 1];
		}
		return ByteBuffer.wrap( theBigEndian ).getDouble();
	}

	/**
	 * Converts the given value to a byte array in big endian notation. The
	 * length for a long are 64 bits, so the result will be 4 bytes long.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toBigEndianBytes( double aValue ) {
		final byte[] theBytes = new byte[Double.BYTES];
		ByteBuffer.wrap( theBytes ).putDouble( aValue );
		return theBytes;
	}

	/**
	 * Converts the given value to a byte array in big endian notation. The
	 * length for a long are 64 bits, so the result will be 4 bytes long.
	 * 
	 * @param aValue The value for which to get the byte array.
	 * 
	 * @return The according array.
	 */
	public static byte[] toLittleEndianBytes( double aValue ) {
		final byte[] theBytes = new byte[Double.BYTES];
		ByteBuffer.wrap( theBytes ).putDouble( aValue );
		final byte[] theResult = new byte[theBytes.length];
		for ( int i = 0; i < theResult.length; i++ ) {
			if ( theBytes.length > i ) {
				theResult[i] = theBytes[theBytes.length - 1 - i];
			}
			else {
				theResult[i] = 0;
			}
		}
		return theResult;
	}

	// -------------------------------------------------------------------------
	// HEX:
	// -------------------------------------------------------------------------

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 * 
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( byte... aBytes ) {
		return toHexString( true, aBytes );
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 *
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( boolean isPrefixBytes, byte... aBytes ) {
		final StringBuilder theBuffer = new StringBuilder();
		for ( byte aByte : aBytes ) {
			theBuffer.append( toHexString( aByte, isPrefixBytes ) );
		}
		return theBuffer.toString();
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 *
	 * @param aBytesPerLine The number of bytes to put on one line before
	 *        inserting a line separator.
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( int aBytesPerLine, byte... aBytes ) {
		return toHexString( aBytesPerLine, true, aBytes );
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 * 
	 * @param aBytesPerLine The number of bytes to put on one line before
	 *        inserting a line separator.
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( int aBytesPerLine, boolean isPrefixBytes, byte... aBytes ) {
		final StringBuilder theBuffer = new StringBuilder();
		for ( int i = 0; i < aBytes.length; i++ ) {
			theBuffer.append( toHexString( aBytes[i], isPrefixBytes ) );
			final boolean hasLineFeed = ( i + 1 ) % aBytesPerLine == 0 && i != aBytes.length - 1;
			if ( hasLineFeed ) {
				theBuffer.append( System.lineSeparator() );
			}
		}
		return theBuffer.toString();
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 *
	 * @param aByteSeparator The separator to separate two hex values from each
	 *        other or null if no separator is to be used.
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( String aByteSeparator, byte... aBytes ) {
		return toHexString( aByteSeparator, true, aBytes );
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 *
	 * @param aByteSeparator The separator to separate two hex values from each
	 *        other or null if no separator is to be used.
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( String aByteSeparator, boolean isPrefixBytes, byte... aBytes ) {
		final StringBuilder theBuffer = new StringBuilder();
		for ( int i = 0; i < aBytes.length; i++ ) {
			theBuffer.append( toHexString( isPrefixBytes, aBytes[i] ) );
			if ( aByteSeparator != null && i < aBytes.length - 1 ) {
				theBuffer.append( aByteSeparator );
			}
		}
		return theBuffer.toString();
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 * 
	 * @param aByteSeparator The separator to separate two hex values from each
	 *        other or null if no separator is to be used.
	 * @param aBytesPerLine The number of bytes to put on one line before
	 *        inserting a line separator.
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( String aByteSeparator, int aBytesPerLine, byte... aBytes ) {
		return toHexString( aByteSeparator, aBytesPerLine, true, aBytes );
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 * 
	 * @param aByteSeparator The separator to separate two hex values from each
	 *        other or null if no separator is to be used.
	 * @param aBytesPerLine The number of bytes to put on one line before
	 *        inserting a line separator.
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( String aByteSeparator, int aBytesPerLine, boolean isPrefixBytes, byte... aBytes ) {
		final StringBuilder theBuffer = new StringBuilder();
		for ( int i = 0; i < aBytes.length; i++ ) {
			theBuffer.append( toHexString( isPrefixBytes, aBytes[i] ) );
			final boolean hasLineFeed = ( i + 1 ) % aBytesPerLine == 0 && i != aBytes.length - 1;
			if ( i < aBytes.length - 1 && !hasLineFeed ) {
				theBuffer.append( aByteSeparator );
			}
			if ( hasLineFeed ) {
				theBuffer.append( System.lineSeparator() );
			}
		}
		return theBuffer.toString();
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 * 
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * @param aByteSeparator The separator to separate two hex values from each
	 *        other or null if no separator is to be used.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( byte[] aBytes, String aByteSeparator ) {
		return toHexString( aBytes, aByteSeparator, true );
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 * 
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * @param aByteSeparator The separator to separate two hex values from each
	 *        other or null if no separator is to be used.
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( byte[] aBytes, String aByteSeparator, boolean isPrefixBytes ) {
		final StringBuilder theBuffer = new StringBuilder();
		for ( int i = 0; i < aBytes.length; i++ ) {
			theBuffer.append( toHexString( isPrefixBytes, aBytes[i] ) );
			if ( aByteSeparator != null && i < aBytes.length - 1 ) {
				theBuffer.append( aByteSeparator );
			}
		}
		return theBuffer.toString();
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 * 
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * @param aBytesPerLine The number of by
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( byte[] aBytes, int aBytesPerLine ) {
		return toHexString( aBytes, aBytesPerLine, true );
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 * 
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * @param aBytesPerLine The number of by
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( byte[] aBytes, int aBytesPerLine, boolean isPrefixBytes ) {
		final StringBuilder theBuffer = new StringBuilder();
		for ( int i = 0; i < aBytes.length; i++ ) {
			theBuffer.append( toHexString( isPrefixBytes, aBytes[i] ) );
			final boolean hasLineFeed = ( i + 1 ) % aBytesPerLine == 0 && i != aBytes.length - 1;
			if ( hasLineFeed ) {
				theBuffer.append( System.lineSeparator() );
			}
		}
		return theBuffer.toString();
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 * 
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * @param aByteSeparator The separator to separate two hex values from each
	 *        other or null if no separator is to be used.
	 * @param aBytesPerLine The number of bytes to put on one line before
	 *        inserting a line separator.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( byte[] aBytes, String aByteSeparator, int aBytesPerLine ) {
		return toHexString( aBytes, aByteSeparator, aBytesPerLine, true );
	}

	/**
	 * Converts the provided bytes to a hexadecimal {@link String}
	 * representation.
	 * 
	 * @param aBytes The bytes for which to get the hexadecimal {@link String}.
	 * @param aByteSeparator The separator to separate two hex values from each
	 *        other or null if no separator is to be used.
	 * @param aBytesPerLine The number of bytes to put on one line before
	 *        inserting a line separator.
	 * @param isPrefixBytes When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( byte[] aBytes, String aByteSeparator, int aBytesPerLine, boolean isPrefixBytes ) {
		final StringBuilder theBuffer = new StringBuilder();
		for ( int i = 0; i < aBytes.length; i++ ) {
			theBuffer.append( toHexString( isPrefixBytes, aBytes[i] ) );
			final boolean hasLineFeed = ( i + 1 ) % aBytesPerLine == 0 && i != aBytes.length - 1;
			if ( i < aBytes.length - 1 && !hasLineFeed ) {
				theBuffer.append( aByteSeparator );
			}
			if ( hasLineFeed ) {
				theBuffer.append( System.lineSeparator() );
			}
		}
		return theBuffer.toString();
	}

	/**
	 * Converts the provided byte (in integer notation as of {@link InputStream}
	 * and {@link OutputStream} integer based byte operations)to a hexadecimal
	 * {@link String} representation.
	 * 
	 * @param aByte The byte (in integer notation) for which to get the
	 *        hexadecimal {@link String}.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( int aByte ) {
		return toHexString( (byte) aByte, true );
	}

	/**
	 * Converts the provided byte (in integer notation as of {@link InputStream}
	 * and {@link OutputStream} integer based byte operations) to a hexadecimal
	 * {@link String} representation.
	 * 
	 * @param aByte The byte (in integer notation) for which to get the
	 *        hexadecimal {@link String}.
	 * @param isPrefixByte When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( int aByte, boolean isPrefixByte ) {
		return toHexString( (byte) aByte, isPrefixByte );
	}

	/**
	 * Converts the provided byte to a hexadecimal {@link String}
	 * representation.
	 * 
	 * @param aByte The byte for which to get the hexadecimal {@link String}.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( byte aByte ) {
		return toHexString( aByte, true );
	}

	/**
	 * Converts the provided byte to a hexadecimal {@link String}
	 * representation.
	 * 
	 * @param aByte The byte for which to get the hexadecimal {@link String}.
	 * @param isPrefixByte When true, then <code>0x</code> is prefixed to the
	 *        HEX values.
	 * 
	 * @return The according {@link String}.
	 */
	public static String toHexString( byte aByte, boolean isPrefixByte ) {
		final StringBuilder theBuffer = new StringBuilder();
		if ( isPrefixByte ) {
			theBuffer.append( "0x" );
		}
		theBuffer.append( Character.forDigit( ( aByte >> 4 ) & 0xF, 16 ) );
		theBuffer.append( Character.forDigit( ( aByte & 0xF ), 16 ) );
		return theBuffer.toString();
	}

	// -------------------------------------------------------------------------
	// BOOLEAN:
	// -------------------------------------------------------------------------

	/**
	 * Converts the given String to a boolean value. Accepted values for a
	 * "true" boolean are "1", "on", "yes", "true". Accepted values for a
	 * "false" boolean are "0", "off", "no", "false".
	 * 
	 * @param aValue The text value to be tested whether it represents true or
	 *        false.
	 * 
	 * @return True or false depending on the value passed.n
	 * 
	 * @throws IllegalArgumentException in case neither true nor false could be
	 *         determined.
	 */
	public static boolean toBoolean( String aValue ) {
		for ( String eTrue : BooleanLiterals.TRUE.getNames() ) {
			if ( eTrue.equalsIgnoreCase( aValue ) ) {
				return true;
			}
		}
		for ( String eFalse : BooleanLiterals.FALSE.getNames() ) {
			if ( eFalse.equalsIgnoreCase( aValue ) ) {
				return false;
			}
		}
		throw new IllegalArgumentException( "Unable to determine neither <true> nor <false> for the provided string \"" + aValue + "\"." );
	}

	// -------------------------------------------------------------------------
	// REVERSED:
	// -------------------------------------------------------------------------

	/**
	 * Reverses the given value's bits.
	 * 
	 * @param aValue The value to be reversed.
	 * 
	 * @return The reversed values.
	 */
	static long toReversedLong( long aValue ) {
		long theResult = 0;
		for ( int i = Long.SIZE - 1; i >= 0; i-- ) {
			theResult |= ( aValue & 1 ) << i;
			aValue >>= 1;
		}
		return theResult;
	}

	/**
	 * Reverses the given value's bits.
	 * 
	 * @param aValue The value to be reversed.
	 * @param aLength The number of bits to be reversed.
	 * 
	 * @return The reversed values.
	 */
	static long toReversedLong( long aValue, int aLength ) {
		long theResult = 0;
		for ( int i = aLength - 1; i >= 0; i-- ) {
			theResult |= ( aValue & 1 ) << i;
			aValue >>= 1;
		}
		return theResult;
	}

	/**
	 * Reverses the given value's bits.
	 * 
	 * @param aValue The value to be reversed.
	 * 
	 * @return The reversed values.
	 */
	static int toReversedInt( int aValue ) {
		int theResult = 0;
		for ( int i = Integer.SIZE - 1; i >= 0; i-- ) {
			theResult |= ( aValue & 1 ) << i;
			aValue >>= 1;
		}
		return theResult;
	}

	/**
	 * Reverses the given value's bits.
	 * 
	 * @param aValue The value to be reversed.
	 * @param aLength The number of bits to be reversed.
	 * 
	 * @return The reversed values.
	 */
	static int toReversedInt( int aValue, int aLength ) {
		int theResult = 0;
		for ( int i = aLength - 1; i >= 0; i-- ) {
			theResult |= ( aValue & 1 ) << i;
			aValue >>= 1;
		}
		return theResult;
	}

	// -------------------------------------------------------------------------
	// OTHER:
	// -------------------------------------------------------------------------

	/**
	 * Generates a md5 hash of the given text.
	 * 
	 * @param aText The text to be md5 hashed.
	 * 
	 * @return The md5 hashed text.
	 * 
	 * @throws NoSuchAlgorithmException thrown in case the <code>MD5</code>
	 *         algorithm cannot be found on the classpath.
	 */
	public static String toMd5Hash( String aText ) throws NoSuchAlgorithmException {
		final byte[] theBytes = aText.getBytes();
		final MessageDigest theMessageDigest = MessageDigest.getInstance( Encoding.MD5.getCode() );
		theMessageDigest.update( theBytes, 0, theBytes.length );
		final String theHash = new BigInteger( 1, theMessageDigest.digest() ).toString( 32 );
		return theHash;
		//	byte[] theBytes = aText.getBytes( "UTF-8" );
		//	MessageDigest theMessageDigest = MessageDigest.getInstance( "MD5" );
		//	byte[] theDigest = theMessageDigest.digest( theBytes );
		//	return Base64.encodeToString( theDigest );
	}

	/**
	 * Scales the given value (which must be between 0 .. 1 to be between aMin
	 * .. aMax.
	 * 
	 * @param aValue The value between 0 .. 1 to be scaled.
	 * @param aMin The min value.
	 * @param aMax The max value.
	 * 
	 * @return A value between aMin ... aMax.
	 */
	public static double toScaled( double aValue, double aMin, double aMax ) {
		if ( aValue < 0 || aValue > 1 ) {
			throw new IllegalArgumentException( "The provided value <" + aValue + "> must be between 0 and 1!" );
		}
		final double theRange = aMax - aMin;
		final double theScaled = aValue * theRange;
		final double theShifted = theScaled + aMin;
		return theShifted;
	}

	// /////////////////////////////////////////////////////////////////////////
	// FUNCTIONS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Calculates the sum from the provided summands.
	 * 
	 * @param aSummands The summands to be added up.
	 * 
	 * @return The sum of the provided summands.
	 */
	public static int sum( int... aSummands ) {
		int theSum = 0;
		for ( int eSummand : aSummands ) {
			theSum += eSummand;
		}
		return theSum;
	}

	/**
	 * Converts the given integer value to a double of the given range.
	 * 
	 * @param aIntValue The integer value which to convert accordingly
	 * @param aMinDouble The lower bounds of the double value.
	 * @param aMaxDouble The upper bounds of the double value.
	 * 
	 * @return The accordingly converted value.
	 */
	public static double toDouble( int aIntValue, double aMinDouble, double aMaxDouble ) {
		final double theRange = aMaxDouble - aMinDouble;
		final long theUnsignedInt = Integer.toUnsignedLong( aIntValue );
		final long theUnsignedIntMax = ( (long) Integer.MIN_VALUE * -1 ) + ( Integer.MAX_VALUE );
		final double theDoubleValue = ( (double) theUnsignedInt ) / ( (double) theUnsignedIntMax ) * theRange;
		return theDoubleValue + aMinDouble;
	}

	/**
	 * Converts the given long value to a double of the given range.
	 * 
	 * @param aLongValue The integer value which to convert accordingly
	 * @param aMinDouble The lower bounds of the double value.
	 * @param aMaxDouble The upper bounds of the double value.
	 * 
	 * @return The accordingly converted value.
	 */
	public static double toDouble( long aLongValue, double aMinDouble, double aMaxDouble ) {
		final BigDecimal theRange = BigDecimal.valueOf( aMaxDouble ).subtract( BigDecimal.valueOf( aMinDouble ) );
		BigDecimal theUnsignedLong = BigDecimal.valueOf( aLongValue );
		theUnsignedLong = theUnsignedLong.add( BigDecimal.valueOf( Long.MAX_VALUE ) ).add( BigDecimal.ONE );
		final BigDecimal theUnsignedLongMax = BigDecimal.valueOf( Long.MIN_VALUE ).abs().add( BigDecimal.valueOf( Long.MAX_VALUE ) );
		final BigDecimal theDivide = theUnsignedLong.divide( theUnsignedLongMax, 16, RoundingMode.HALF_DOWN );
		final BigDecimal theDoubleValue = theDivide.multiply( theRange );
		return theDoubleValue.add( BigDecimal.valueOf( aMinDouble ) ).doubleValue();
	}

	// /////////////////////////////////////////////////////////////////////////
	// CODES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Calculates a Java-Version's implementation independent Hash-Code.
	 * 
	 * @param aValue The {@link String} from which to get the Hash-Code.
	 * 
	 * @return The according Hash-Code.
	 */
	public static int toHashCode( String aValue ) {
		int theHash = 0;
		final byte[] theBytes = aValue.getBytes();
		for ( byte theByte : theBytes ) {
			theHash = 31 * theHash + theByte;
		}
		return theHash;
	}

	/**
	 * Creates a given number of IDs from the provided source text. Useful when
	 * creating numbers from a pass-phrase.
	 * 
	 * @param aSource The source text from which to generate the IDs.
	 * @param aCount The number of IDs to generate.
	 * 
	 * @return The IDs being generated.
	 */
	public static int[] toHashCodes( String aSource, int aCount ) {
		final StringBuilder[] theBuilders = new StringBuilder[aCount];
		for ( int i = 0; i < theBuilders.length; i++ ) {
			theBuilders[i] = new StringBuilder();
		}
		final int theSegmentSize = ( aSource.length() / aCount ) + aCount;
		int ePos;
		for ( int i = 0; i < theSegmentSize; i++ ) {
			for ( int j = 0; j < theBuilders.length; j++ ) {
				ePos = ( j * theSegmentSize + i ) % aSource.length();
				theBuilders[j].append( aSource.charAt( ePos ) );
			}
		}
		final int[] theResult = new int[aCount];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = toHashCode( theBuilders[i].toString() );
		}
		return theResult;
	}

	/**
	 * Creates a given number of IDs from the provided source text. Useful when
	 * creating numbers from a pass-phrase.
	 * 
	 * @param aSource The source text from which to generate the IDs.
	 * @param aCount The number of IDs to generate.
	 * 
	 * @return The IDs being generated.
	 */
	public static int[] toHashCodes( char[] aSource, int aCount ) {
		final StringBuilder[] theBuilders = new StringBuilder[aCount];
		for ( int i = 0; i < theBuilders.length; i++ ) {
			theBuilders[i] = new StringBuilder();
		}
		final int theSegmentSize = ( aSource.length / aCount ) + aCount;
		int ePos;
		for ( int i = 0; i < theSegmentSize; i++ ) {
			for ( int j = 0; j < theBuilders.length; j++ ) {
				ePos = ( j * theSegmentSize + i ) % aSource.length;
				theBuilders[j].append( aSource[ePos] );
			}
		}
		final int[] theResult = new int[aCount];
		for ( int i = 0; i < theResult.length; i++ ) {
			theResult[i] = toHashCode( theBuilders[i].toString() );
		}
		return theResult;
	}
}