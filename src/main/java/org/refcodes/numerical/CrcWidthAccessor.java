// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

/**
 * Provides an accessor for a CRC byte width (number of bytes used to store a
 * CRC checksum) property.
 */
public interface CrcWidthAccessor {

	/**
	 * Retrieves the value from the CRC byte width (number of bytes used to
	 * store a CRC checksum) property.
	 * 
	 * @return The name stored by the CRC byte width (number of bytes used to
	 *         store a CRC checksum) property.
	 */
	int getCrcWidth();

	/**
	 * Provides a mutator for a CRC byte width (number of bytes used to store a
	 * CRC checksum) property.
	 */
	public interface CrcWidthMutator {

		/**
		 * Sets the value for the CRC byte width (number of bytes used to store
		 * a CRC checksum) property.
		 * 
		 * @param aCrcWidth The value to be stored by the CRC byte width (number
		 *        of bytes used to store a CRC checksum) property.
		 */
		void setCrcWidth( int aCrcWidth );
	}

	/**
	 * Provides a builder method for a CRC byte width (number of bytes used to
	 * store a CRC checksum) property returning the builder for applying
	 * multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface CrcWidthBuilder<B extends CrcWidthBuilder<B>> {

		/**
		 * Sets the value for the CRC byte width (number of bytes used to store
		 * a CRC checksum) property.
		 * 
		 * @param aCrcWidth The value to be stored by the CRC byte width (number
		 *        of bytes used to store a CRC checksum) property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCrcWidth( int aCrcWidth );
	}

	/**
	 * Provides a CRC byte width (number of bytes used to store a CRC checksum)
	 * property.
	 */
	public interface CrcWidthProperty extends CrcWidthAccessor, CrcWidthMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setCrcWidth(int)} and returns the very same value (getter).
		 * 
		 * @param aCrcWidth The integer to set (via {@link #setCrcWidth(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letCrcWidth( int aCrcWidth ) {
			setCrcWidth( aCrcWidth );
			return aCrcWidth;
		}
	}
}
