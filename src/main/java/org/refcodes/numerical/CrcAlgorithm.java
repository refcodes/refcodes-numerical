// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

import org.refcodes.mixin.NameAccessor;

/**
 * This interfaces describes the methods which can be invoked upon working with
 * CRC checksums.
 */
public interface CrcAlgorithm extends CrcWidthAccessor, NameAccessor {

	/**
	 * Gets the {@link CrcSize} class to which this {@link CrcAlgorithm}
	 * belongs.
	 * 
	 * @return The {@link CrcSize} enumeration providing various width related
	 *         methods.
	 */
	CrcSize getCrcSize();

	/**
	 * Calculates the according checksum from the supplied data.
	 * 
	 * @param aData The data for which to calculate the checksum.
	 * 
	 * @return The according checksum.
	 */
	long toCrcChecksum( byte aData );

	/**
	 * Updates the according checksum with he supplied data.
	 * 
	 * @param aCrc The CRC checksum to be updated.
	 * @param aData The data for which to calculate the checksum.
	 * 
	 * @return The accordingly updated checksum.
	 */
	long toCrcChecksum( long aCrc, byte aData );

	/**
	 * Calculates the according checksum from the supplied data.
	 * 
	 * @param aData The data for which to calculate the checksum.
	 * 
	 * @return The according checksum.
	 */
	long toCrcChecksum( byte[] aData );

	/**
	 * Updates the according checksum with he supplied data.
	 * 
	 * @param aCrc The CRC checksum to be updated.
	 * @param aData The data for which to calculate the checksum.
	 * 
	 * @return The accordingly updated checksum.
	 */
	long toCrcChecksum( long aCrc, byte[] aData );

	/**
	 * Calculates the according checksum from the supplied data.
	 * 
	 * @param aData The data for which to calculate the checksum.
	 * @param aOffset The offset from where to start.
	 * @param aLength The number of bytes to cover.
	 * 
	 * @return The according checksum.
	 */
	long toCrcChecksum( byte[] aData, int aOffset, int aLength );

	/**
	 * Updates the according checksum with he supplied data.
	 * 
	 * @param aCrc The CRC checksum to be updated.
	 * @param aData The data for which to calculate the checksum.
	 * @param aOffset The offset from where to start.
	 * @param aLength The number of bytes to cover.
	 * 
	 * @return The accordingly updated checksum.
	 */
	long toCrcChecksum( long aCrc, byte[] aData, int aOffset, int aLength );

	/**
	 * Calculates the according checksum from the supplied data and returns the
	 * according byte array of the CRC checksum.
	 * 
	 * @param aData The data for which to calculate the checksum.
	 * @param aEndianess The {@link Endianess} (big endian or little endian) of
	 *        the resulting bytes representing the CRC checksum.
	 * 
	 * @return The according checksum.
	 */
	byte[] toCrcBytes( byte aData, Endianess aEndianess );

	/**
	 * Updates the according checksum with he supplied data data and returns the
	 * according byte array of the CRC checksum.
	 * 
	 * @param aCrc The CRC checksum to be updated.
	 * @param aData The data for which to calculate the checksum.
	 * @param aEndianess The {@link Endianess} (big endian or little endian) of
	 *        the resulting bytes representing the CRC checksum.
	 * 
	 * @return The accordingly updated checksum.
	 */
	byte[] toCrcBytes( long aCrc, byte aData, Endianess aEndianess );

	/**
	 * Calculates the according checksum from the supplied data data and returns
	 * the according byte array of the CRC checksum.
	 * 
	 * @param aData The data for which to calculate the checksum.
	 * @param aEndianess The {@link Endianess} (big endian or little endian) of
	 *        the resulting bytes representing the CRC checksum.
	 * 
	 * @return The according checksum.
	 */
	byte[] toCrcBytes( byte[] aData, Endianess aEndianess );

	/**
	 * Updates the according checksum with he supplied data data and returns the
	 * according byte array of the CRC checksum.
	 * 
	 * @param aCrc The CRC checksum to be updated.
	 * @param aData The data for which to calculate the checksum.
	 * @param aEndianess The {@link Endianess} (big endian or little endian) of
	 *        the resulting bytes representing the CRC checksum.
	 * 
	 * @return The accordingly updated checksum.
	 */
	byte[] toCrcBytes( long aCrc, byte[] aData, Endianess aEndianess );

	/**
	 * Calculates the according checksum from the supplied data data and returns
	 * the according byte array of the CRC checksum.
	 * 
	 * @param aData The data for which to calculate the checksum.
	 * @param aOffset The offset from where to start.
	 * @param aLength The number of bytes to cover.
	 * @param aEndianess The {@link Endianess} (big endian or little endian) of
	 *        the resulting bytes representing the CRC checksum.
	 * 
	 * @return The according checksum.
	 */
	byte[] toCrcBytes( byte[] aData, int aOffset, int aLength, Endianess aEndianess );

	/**
	 * Updates the according checksum with he supplied data data and returns the
	 * according byte array of the CRC checksum.
	 * 
	 * @param aCrc The CRC checksum to be updated.
	 * @param aData The data for which to calculate the checksum.
	 * @param aOffset The offset from where to start.
	 * @param aLength The number of bytes to cover.
	 * @param aEndianess The {@link Endianess} (big endian or little endian) of
	 *        the resulting bytes representing the CRC checksum.
	 * 
	 * @return The accordingly updated checksum.
	 */
	byte[] toCrcBytes( long aCrc, byte[] aData, int aOffset, int aLength, Endianess aEndianess );

}