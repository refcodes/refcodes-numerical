// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

// -----------------------------------------------------------------------------
// The following note is provided as of the code in `CrcAlgorithm` has been
// copy'n'pasted from the "Online CRC-8 CRC-16 CRC-32 Calculator"
// (https://crccalc.com)'s GitHub "https://github.com/meetanthony/crcjava.git"
// repository:
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// The MIT License (MIT)
//
// Copyright (c) 2017 Anton Isakov http://crccalc.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// -----------------------------------------------------------------------------

package org.refcodes.numerical;

import java.util.ArrayList;
import java.util.List;

/**
 * Enumeration with CRC implementations hacked together from sources "out
 * there".
 */
public enum CrcSize implements CrcWidthAccessor {

	// @formatter:off
	CRC_8(1), CRC_16(2), CRC_32(4), CRC_64(8);
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _crcByteWidth;
	private CrcStandard[] _crcAlgorithms;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private CrcSize( int aCrcByteWidth ) {
		final List<CrcStandard> theCrcAlgorithms = new ArrayList<>();
		for ( CrcStandard eCrcAlgorithm : CrcStandard.values() ) {
			if ( eCrcAlgorithm.getCrcWidth() == aCrcByteWidth ) {
				theCrcAlgorithms.add( eCrcAlgorithm );
			}
		}
		_crcAlgorithms = theCrcAlgorithms.toArray( new CrcStandard[theCrcAlgorithms.size()] );
		_crcByteWidth = aCrcByteWidth;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCrcWidth() {
		return _crcByteWidth;
	}

	/**
	 * Retrieves the {@link CrcStandard} implementations as of the according
	 * {@link CrcSize}.
	 * 
	 * @return The {@link CrcStandard} implementations with the according
	 *         checksum's "CRC byte length"
	 */
	public CrcStandard[] getCrcAlgorithms() {
		return _crcAlgorithms;
	}

	/**
	 * Retrieves the {@link CrcSize} representing the queried "byte length".
	 * 
	 * @param aCrcByteWidth The "byte length" for which to retrieve the
	 *        according {@link CrcSize}.
	 * 
	 * @return The according {@link CrcSize} or null if none fitting was found.
	 */
	public static CrcSize toCrcCategory( int aCrcByteWidth ) {
		for ( CrcSize eCrcWidth : values() ) {
			if ( eCrcWidth.getCrcWidth() == aCrcByteWidth ) {
				return eCrcWidth;
			}
		}
		return null;
	}
}