// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

/**
 * Provides an accessor for a number base property.
 */
public interface NumberBaseAccessor {

	/**
	 * Retrieves the value from the number base property.
	 * 
	 * @return The name stored by the number base property.
	 */
	int getNumberBase();

	/**
	 * Provides a mutator for a number base property.
	 */
	public interface NumberBaseMutator {

		/**
		 * Sets the value for the number base property.
		 * 
		 * @param aNumberBase The value to be stored by the number base
		 *        property.
		 */
		void setNumberBase( int aNumberBase );
	}

	/**
	 * Provides a builder method for a number base property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface NumberBaseBuilder<B extends NumberBaseBuilder<B>> {

		/**
		 * Sets the value for the number base property.
		 * 
		 * @param aNumberBase The value to be stored by the number base
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withNumberBase( int aNumberBase );
	}

	/**
	 * Provides a number base property.
	 */
	public interface NumberBaseProperty extends NumberBaseAccessor, NumberBaseMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer (setter) as of
		 * {@link #setNumberBase(int)} and returns the very same value (getter).
		 * 
		 * @param aNumberBase The integer to set (via
		 *        {@link #setNumberBase(int)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int letNumberBase( int aNumberBase ) {
			setNumberBase( aNumberBase );
			return aNumberBase;
		}
	}
}
