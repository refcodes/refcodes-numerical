// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

/**
 * A {@link BijectiveFunction} provides methods to apply a bijective function
 * (on the inverse function's return vale).
 * 
 * @param <B> The type of the bijective function's result.
 * @param <I> The type of the bijective function's input (being the inverse
 *        function's output type).
 */
@FunctionalInterface
public interface BijectiveFunction<B, I> {

	/**
	 * Applies the bijective function to the given value.
	 *
	 * @param aValue The (invertion's) value.
	 * 
	 * @return The according bijection.
	 */
	B applyBijection( I aValue );

}