// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

import org.refcodes.mixin.ConcatenateMode;

/**
 * A {@link AbstractCrcMixin} provides a class based implementation of the
 * {@link CrcMixin} for CRC checksum creation and validation of accompanied
 * data.
 */
public abstract class AbstractCrcMixin implements CrcMixin {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected CrcAlgorithm _crcAlgorithm;
	protected ChecksumValidationMode _checksumValidationMode;
	protected ConcatenateMode _crcChecksumConcatenateMode;
	protected Endianess _endianess;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according instance from the given arguments.
	 * 
	 * @param aCrcAlgorithm The {@link CrcAlgorithm} to be used for CRC checksum
	 *        calculation.
	 * @param aCrcChecksumConcatenateMode The mode of concatenation to use when
	 *        enriching data with a CRC checksum.
	 * @param aChecksumValidationMode The mode of operation when validating
	 *        provided CRC checksums against calculated ones.
	 * @param aEndianess The {@link Endianess} to use when calculating the CRC
	 *        checksum.
	 */
	public AbstractCrcMixin( CrcAlgorithm aCrcAlgorithm, ConcatenateMode aCrcChecksumConcatenateMode, ChecksumValidationMode aChecksumValidationMode, Endianess aEndianess ) {
		_crcAlgorithm = aCrcAlgorithm;
		_checksumValidationMode = aChecksumValidationMode;
		_crcChecksumConcatenateMode = aCrcChecksumConcatenateMode;
		_endianess = aEndianess;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConcatenateMode getCrcChecksumConcatenateMode() {
		return _crcChecksumConcatenateMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ChecksumValidationMode getChecksumValidationMode() {
		return _checksumValidationMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CrcAlgorithm getCrcAlgorithm() {
		return _crcAlgorithm;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Endianess getEndianess() {
		return _endianess;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _crcChecksumConcatenateMode == null ) ? 0 : _crcChecksumConcatenateMode.hashCode() );
		result = prime * result + ( ( _crcAlgorithm == null ) ? 0 : _crcAlgorithm.hashCode() );
		result = prime * result + ( ( _checksumValidationMode == null ) ? 0 : _checksumValidationMode.hashCode() );
		result = prime * result + ( ( _endianess == null ) ? 0 : _endianess.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final AbstractCrcMixin other = (AbstractCrcMixin) obj;
		if ( _crcChecksumConcatenateMode != other._crcChecksumConcatenateMode ) {
			return false;
		}
		if ( _crcAlgorithm == null ) {
			if ( other._crcAlgorithm != null ) {
				return false;
			}
		}
		else if ( !_crcAlgorithm.equals( other._crcAlgorithm ) ) {
			return false;
		}
		if ( _checksumValidationMode != other._checksumValidationMode ) {
			return false;
		}
		if ( _endianess != other._endianess ) {
			return false;
		}
		return true;
	}
}
