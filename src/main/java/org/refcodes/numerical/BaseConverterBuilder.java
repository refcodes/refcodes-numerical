// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

import org.refcodes.data.CharSet;
import org.refcodes.mixin.CharSetAccessor.CharSetBuilder;
import org.refcodes.mixin.CharSetAccessor.CharSetProperty;
import org.refcodes.numerical.NumberBaseAccessor.NumberBaseBuilder;
import org.refcodes.numerical.NumberBaseAccessor.NumberBaseProperty;

/**
 * A base converter from and to long values and which may also convert to and
 * from byte arrays. You set a number base via {@link #withNumberBase(int)} or
 * {@link #setNumberBase(int)} and you can convert forth and back with
 * {@link #toDigits(long)} or {@link #toNumber(String)}.
 */
public class BaseConverterBuilder implements CharSetProperty, CharSetBuilder<BaseConverterBuilder>, NumberBaseProperty, NumberBaseBuilder<BaseConverterBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int DECIMAL_NUMBER_BASE = 10;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _numberBase = DECIMAL_NUMBER_BASE;
	private String _digits = null;
	private long _number;
	private char[] _charSet = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new base converter builder impl.
	 */
	public BaseConverterBuilder() {
		setCharSet( CharSet.ARABIC_BASE64.getCharSet() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char[] getCharSet() {
		return _charSet;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCharSet( char[] aCharSet ) {
		_charSet = aCharSet;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BaseConverterBuilder withCharSet( char[] aCharSet ) {
		setCharSet( aCharSet );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setNumberBase( int aNumberBase ) {
		_numberBase = aNumberBase;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getNumberBase() {
		return _numberBase;
	}

	/**
	 * Retrieves the number base value calculated from the number. This method
	 * is to be side effect free in terms of the number (and the encoded result)
	 * is not part of the state for this instance (from the point of view of
	 * this method).
	 * 
	 * @param aNumber The number to be encoded.
	 * 
	 * @return The number base value calculated from the number.
	 */
	public String toDigits( long aNumber ) {
		return asEncoded( aNumber, _numberBase, _charSet );
	}

	/**
	 * Retrieves the number from the number property.
	 * 
	 * @return The number stored by the number property.
	 */
	public long getNumber() {
		return _number;
	}

	/**
	 * Retrieves the number calculated from the provided number base value. This
	 * method is to be side effect free in terms of the number base value (and
	 * the decoded result) is not part of the state for this instance (from the
	 * point of view of this method).
	 *
	 * @param aDigits The number base value to be decoded.
	 * 
	 * @return The number decoded from the number base value.
	 * 
	 * @throws IllegalArgumentException the illegal argument exception
	 */
	public long toNumber( String aDigits ) {
		return asDecoded( aDigits, _numberBase, _charSet );
	}

	/**
	 * Sets the number for the number property.
	 * 
	 * @param aNumber The number to be stored by the number property.
	 */
	public void setNumber( long aNumber ) {
		_number = aNumber;

	}

	/**
	 * Retrieves the number base value from the number base value property.
	 * 
	 * @return The number base value stored by the number base value property.
	 */
	public String getDigits() {
		_digits = asEncoded( _number, _numberBase, _charSet );
		return _digits;
	}

	/**
	 * Sets the number base value for the number base value property.
	 * 
	 * @param aDigits The number base value to be stored by the number base
	 *        value property.
	 */
	public void setDigits( String aDigits ) {
		_digits = aDigits;
		_number = asDecoded( aDigits, _numberBase, _charSet );
	}

	/**
	 * Sets the number base for the number base property.
	 * 
	 * @param aNumberBase The number base to be stored by the number base
	 *        property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	@Override
	public BaseConverterBuilder withNumberBase( int aNumberBase ) {
		setNumberBase( aNumberBase );
		return this;
	}

	/**
	 * Sets the number base value for the number base value property.
	 * 
	 * @param aDigits The number base value to be stored by the number base
	 *        value property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public BaseConverterBuilder withDigits( String aDigits ) {
		setDigits( aDigits );
		return this;
	}

	/**
	 * Sets the number for the number property.
	 * 
	 * @param aNumber The number to be stored by the number property.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public BaseConverterBuilder withNumber( long aNumber ) {
		setNumber( aNumber );
		return this;
	}

	/**
	 * Encodes the given bytes to a {@link String} as of the given number base
	 * and the charset for the value's digits.
	 *
	 * @param aNumber The data to be encoded.
	 * @param aNumberBase The number base to be used for encoding.
	 * @param aCharSet The charset to be used for the digits of the targeted
	 *        number base.
	 * 
	 * @return Returns the accordingly encoded data as a {@link String}.
	 */
	public static String asEncoded( long aNumber, int aNumberBase, char... aCharSet ) {
		return NumericalUtility.toNumberBase( aNumber, aNumberBase, aCharSet );
	}

	/**
	 * Decodes the given (number base encoded) data to a byte array as of the
	 * given number base and the charset for the value's digits.
	 *
	 * @param aDigits The (number base encoded) data to be decoded.
	 * @param aNumberBase The number base to be used for decoding.
	 * @param aCharSet The charset to be used for the digits of the used number
	 *        base.
	 * 
	 * @return Returns the accordingly decoded data .
	 */
	public static long asDecoded( String aDigits, int aNumberBase, char... aCharSet ) {
		return NumericalUtility.fromNumberBase( aNumberBase, aCharSet, aDigits );
	}
}
