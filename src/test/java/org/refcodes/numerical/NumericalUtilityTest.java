// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.UUID;
import org.junit.jupiter.api.Test;

public class NumericalUtilityTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testNumberBaseCOnversion() {
		final String theFromDigits = "FF";
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theFromDigits );
		}
		final String theToDigits = NumericalUtility.convertNumberBase( 16, theFromDigits, 2 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theToDigits );
		}
		final long theValue = NumericalUtility.fromNumberBase( 2, theToDigits );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theValue );
		}
		assertEquals( 255, theValue );
		final String theResult = NumericalUtility.toNumberBase( theValue, 16 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theResult );
		}
		assertEquals( theFromDigits, theResult );
	}

	@Test
	public void testSetBits1() {
		byte theValue = 0;
		assertFalse( NumericalUtility.isBitSetAt( theValue, 0 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 1 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 2 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 3 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 4 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 5 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 6 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 7 ) );
		theValue = NumericalUtility.setBitAt( theValue, 0, true );
		theValue = NumericalUtility.setBitAt( theValue, 1, true );
		theValue = NumericalUtility.setBitAt( theValue, 2, true );
		theValue = NumericalUtility.setBitAt( theValue, 3, true );
		theValue = NumericalUtility.setBitAt( theValue, 4, true );
		theValue = NumericalUtility.setBitAt( theValue, 5, true );
		theValue = NumericalUtility.setBitAt( theValue, 6, true );
		theValue = NumericalUtility.setBitAt( theValue, 7, true );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theValue );
		}
		assertTrue( NumericalUtility.isBitSetAt( theValue, 0 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 1 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 2 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 3 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 4 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 5 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 6 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 7 ) );
		assertEquals( (byte) 0xFF, theValue );
	}

	@Test
	public void testSetBits2() {
		byte theValue = (byte) 0xFF;
		assertTrue( NumericalUtility.isBitSetAt( theValue, 0 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 1 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 2 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 3 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 4 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 5 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 6 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 7 ) );
		theValue = NumericalUtility.setBitAt( theValue, 0, false );
		theValue = NumericalUtility.setBitAt( theValue, 1, false );
		theValue = NumericalUtility.setBitAt( theValue, 2, false );
		theValue = NumericalUtility.setBitAt( theValue, 3, false );
		theValue = NumericalUtility.setBitAt( theValue, 4, false );
		theValue = NumericalUtility.setBitAt( theValue, 5, false );
		theValue = NumericalUtility.setBitAt( theValue, 6, false );
		theValue = NumericalUtility.setBitAt( theValue, 7, false );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theValue );
		}
		assertFalse( NumericalUtility.isBitSetAt( theValue, 0 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 1 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 2 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 3 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 4 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 5 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 6 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 7 ) );
		assertEquals( 0, theValue );
	}

	@Test
	public void testSetBits3() {
		byte theValue = (byte) 0xFF;
		assertTrue( NumericalUtility.isBitSetAt( theValue, 0 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 1 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 2 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 3 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 4 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 5 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 6 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 7 ) );
		theValue = NumericalUtility.setBitAt( theValue, 0, true );
		theValue = NumericalUtility.setBitAt( theValue, 1, false );
		theValue = NumericalUtility.setBitAt( theValue, 2, true );
		theValue = NumericalUtility.setBitAt( theValue, 3, false );
		theValue = NumericalUtility.setBitAt( theValue, 4, true );
		theValue = NumericalUtility.setBitAt( theValue, 5, false );
		theValue = NumericalUtility.setBitAt( theValue, 6, true );
		theValue = NumericalUtility.setBitAt( theValue, 7, false );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theValue );
		}
		assertTrue( NumericalUtility.isBitSetAt( theValue, 0 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 1 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 2 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 3 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 4 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 5 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 6 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 7 ) );
		assertEquals( 85, theValue );
	}

	@Test
	public void testSetBits4() {
		byte theValue = 0;
		assertFalse( NumericalUtility.isBitSetAt( theValue, 0 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 1 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 2 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 3 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 4 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 5 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 6 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 7 ) );
		theValue = NumericalUtility.setBitAt( theValue, 0, true );
		theValue = NumericalUtility.setBitAt( theValue, 1, false );
		theValue = NumericalUtility.setBitAt( theValue, 2, true );
		theValue = NumericalUtility.setBitAt( theValue, 3, false );
		theValue = NumericalUtility.setBitAt( theValue, 4, true );
		theValue = NumericalUtility.setBitAt( theValue, 5, false );
		theValue = NumericalUtility.setBitAt( theValue, 6, true );
		theValue = NumericalUtility.setBitAt( theValue, 7, false );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theValue );
		}
		assertTrue( NumericalUtility.isBitSetAt( theValue, 0 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 1 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 2 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 3 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 4 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 5 ) );
		assertTrue( NumericalUtility.isBitSetAt( theValue, 6 ) );
		assertFalse( NumericalUtility.isBitSetAt( theValue, 7 ) );
		assertEquals( 85, theValue );
	}

	@Test
	public void testToBytes1() {
		final byte[] theBytes = { 0, 1, 2, 3, 127, (byte) 128, (byte) 129, (byte) 253, (byte) 254, (byte) 255 };
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "[✓] " + Arrays.toString( theBytes ) );
		}
		// @formatter:off
		final String[] theStrings = {
			"{ 0x00 0x01 0x02 0x03 0x7F 0x80 0x81 0xFD 0xFE 0xFF }", 
			"[   0,   1,   2,   3, 127, 128, 129, 253, 254, 255 ]",
			"[   0,   1,   2,   3, 127, 128, 129, -3, -2, -1 ]",
			"( 0    0x01,   2,0x03, 127,0x80, 129 0xFD, 254,0xFF )",
			"0,0x01,2,0x03,127,0x80,129,0xFD,254,0xFF"
		};
		// @formatter:on
		String eString;
		byte[] eBytes;
		for ( int i = 0; i < theStrings.length; i++ ) {
			eString = theStrings[i];
			eBytes = NumericalUtility.toBytes( eString );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "] " + Arrays.toString( eBytes ) );
			}
			assertArrayEquals( theBytes, eBytes );
		}
	}

	@Test
	public void testToBytes2() {
		// @formatter:off
		final String[] theStrings = {
			"{ 0x00 0x01 0x02 0x03 0x7F 0x80 0x81 0xFD 0xFE 0x100 }", 
			"[   0,   1,   X,   3, 127, 128, 129, 253, 254, 255 ]",
			"[   0,   1,   2,   3, 127, 128, 129, -3, -2, -129 ]",
			"( !0    0x01,   2,0x03, 127,0x80, 129 0xFD, 254,0xFF )",
			"0,0x01,2,0x03,127,0x80,129,0xFD,254,-256"
		};
		// @formatter:on
		String eString;
		byte[] eBytes;
		for ( int i = 0; i < theStrings.length; i++ ) {
			eString = theStrings[i];
			try {
				eBytes = NumericalUtility.toBytes( eString );
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "[" + i + "] " + eString );
					System.out.println( "[x] " + Arrays.toString( eBytes ) );
				}
				fail( "Expected an <IllegalArgumentException> (<NumberFormatException>!" );
			}
			catch ( IllegalArgumentException expected ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( "[" + i + "] " + expected.getMessage() );
				}
			}
		}
	}

	@Test
	public void testEdgeCase() {
		final long theEdgeCase = 0xff9833db2bcc861dL;
		final byte[] theBytes = NumericalUtility.toUnsignedBigEndianBytes( theEdgeCase, 8 );
		final long theConversion = NumericalUtility.toLongFromBigEndianBytes( theBytes );
		System.out.println( "   theEdgeCase = " + theEdgeCase );
		System.out.println( "theEdgeCaseHex = " + Long.toHexString( theEdgeCase ) );
		System.out.println( "      theBytes = " + NumericalUtility.toHexString( theBytes ) );
		System.out.println( " theConversion = " + Long.toHexString( theConversion ) );
	}

	@Test
	public void testToLittleEndian() {
		// 16535 = 01000000 10010111 in big endian 
		final byte[] theLittleEndian = new byte[2];
		theLittleEndian[0] = (byte) Integer.parseInt( "10010111", 2 );
		theLittleEndian[1] = (byte) Integer.parseInt( "01000000", 2 );
		final byte[] theResult = NumericalUtility.toLittleEndianBytes( 16535, 2 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Expected = " + Arrays.toString( theLittleEndian ) );
			System.out.println( "  Result = " + Arrays.toString( theResult ) );
		}
		assertArrayEquals( theLittleEndian, theResult );
	}

	@Test
	public void testToBigEndian() {
		// 16535 = 01000000 10010111 in big endian 
		final byte[] theLittleEndian = new byte[2];
		theLittleEndian[0] = (byte) Integer.parseInt( "01000000", 2 );
		theLittleEndian[1] = (byte) Integer.parseInt( "10010111", 2 );
		final byte[] theResult = NumericalUtility.toBigEndianBytes( 16535, 2 );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Expected = " + Arrays.toString( theLittleEndian ) );
			System.out.println( "  Result = " + Arrays.toString( theResult ) );
		}
		assertArrayEquals( theLittleEndian, theResult );
	}

	@Test
	public void testToDouble() {
		Double eDouble;
		String eText;
		double theMin = -1;
		double theMax = 0;
		for ( int i = 0; i < 15000; i++ ) {
			for ( int j = 1; j < 30; j++ ) {
				// eText = RandomStringUtils.random( j );
				eText = toRandomText( j );
				eDouble = NumericalUtility.toDouble( eText );
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eDouble + " := " + eText );
				}
				assertTrue( eDouble >= 0 );
				assertTrue( eDouble <= 1 );
				if ( theMin == -1 || theMin > eDouble ) {
					theMin = eDouble;
				}
				if ( theMax < eDouble ) {
					theMax = eDouble;
				}
			}
		}
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Min := " + theMin );
			System.out.println( "Max := " + theMax );
		}
	}

	@Test
	public void testToDoubleMin() {
		final byte[] theBytes = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 };
		final String theText = new String( theBytes );
		final double theDouble = NumericalUtility.toDouble( theText );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theDouble + " := " + theText );
		}
	}

	@Test
	public void testToDoubleMax() {
		final byte[] theBytes = new byte[] { (byte) 0xff };
		final String theText = new String( theBytes );
		final double theDouble = NumericalUtility.toDouble( theText );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theDouble + " := " + theText );
		}
	}

	@Test
	public void testToDoubles1() {
		String eString = "";
		for ( int l = 1; l < 100; l++ ) {
			for ( int i = 1; i < 100; i++ ) {
				try {
					NumericalUtility.toDoubles( eString, i );
				}
				catch ( Exception e ) {
					fail( "Expecting not to fail at length <" + i + ">! Message = " + e.getMessage() );
				}
			}
			// eString = RandomStringUtils.randomAlphabetic( l );
			eString = toRandomText( l );
		}
	}

	@Test
	public void testToDoubles2() {
		final double[] theDoubles = NumericalUtility.toDoubles( "Hallo Welt, Chais is here to stay!", 3 );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( double eDouble : theDoubles ) {
				System.out.println( eDouble );
			}
		}
	}

	@Test
	public void testToDoubles3() {
		final double[] theDoubles = NumericalUtility.toDoubles( "08021972", 3 );
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( double eDouble : theDoubles ) {
				System.out.println( eDouble );
			}
		}
	}

	@Test
	public void testLongToBytes() {
		final long theLong = 1234567890;
		final byte[] theBytes = NumericalUtility.toBytes( theLong );
		final long theResult = NumericalUtility.toLong( theBytes );
		assertEquals( theLong, theResult );
	}

	@Test
	public void testLongToBytesZero() {
		final long theLong = 0;
		final byte[] theBytes = NumericalUtility.toBytes( theLong );
		final long theResult = NumericalUtility.toLong( theBytes );
		assertEquals( theLong, theResult );
	}

	@Test
	public void testLongToBytesMin() {
		final long theLong = Long.MIN_VALUE;
		final byte[] theBytes = NumericalUtility.toBytes( theLong );
		final long theResult = NumericalUtility.toLong( theBytes );
		assertEquals( theLong, theResult );
	}

	@Test
	public void testLongToBytesMax() {
		final long theLong = Long.MAX_VALUE;
		final byte[] theBytes = NumericalUtility.toBytes( theLong );
		final long theResult = NumericalUtility.toLong( theBytes );
		assertEquals( theLong, theResult );
	}

	@Test
	public void testIntegerToBytes() {
		final int theInteger = 1234567890;
		final byte[] theBytes = NumericalUtility.toBytes( theInteger );
		final int theResult = NumericalUtility.toInt( theBytes );
		assertEquals( theInteger, theResult );
	}

	@Test
	public void testIntegerToBytesZero() {
		final int theInteger = 0;
		final byte[] theBytes = NumericalUtility.toBytes( theInteger );
		final int theResult = NumericalUtility.toInt( theBytes );
		assertEquals( theInteger, theResult );
	}

	@Test
	public void testIntegerToBytesMin() {
		final int theInteger = Integer.MIN_VALUE;
		final byte[] theBytes = NumericalUtility.toBytes( theInteger );
		final int theResult = NumericalUtility.toInt( theBytes );
		assertEquals( theInteger, theResult );
	}

	@Test
	public void testIntegerToBytesMax() {
		final int theInteger = Integer.MAX_VALUE;
		final byte[] theBytes = NumericalUtility.toBytes( theInteger );
		final int theResult = NumericalUtility.toInt( theBytes );
		assertEquals( theInteger, theResult );
	}

	@Test
	public void testToFromBigEndianBytes() {
		byte[] eBytes;
		int eValue;
		for ( int i = -1024; i < 1024; i++ ) {
			eBytes = NumericalUtility.toBigEndianBytes( i, 8 );
			eValue = NumericalUtility.toIntFromBigEndianBytes( eBytes );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + i + "-?->" + eValue );
			}
			assertEquals( i, eValue );
		}
		// Integer MAX & MIN |-->
		int theValue = Integer.MAX_VALUE;
		eBytes = NumericalUtility.toBigEndianBytes( theValue, 8 );
		eValue = NumericalUtility.toIntFromBigEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theValue + "-?->" + eValue );
		}
		assertEquals( theValue, eValue );
		theValue = Integer.MIN_VALUE;
		eBytes = NumericalUtility.toBigEndianBytes( theValue, 8 );
		eValue = NumericalUtility.toIntFromBigEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theValue + "-?->" + eValue );
		}
		assertEquals( theValue, eValue );
		// Long MAX & MIN |-->
		long theLong = Long.MAX_VALUE;
		eBytes = NumericalUtility.toBigEndianBytes( theLong, 8 );
		long eLong = NumericalUtility.toLongFromBigEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theLong + "-?->" + eLong );
		}
		assertEquals( theLong, eLong );
		theLong = Long.MIN_VALUE;
		eBytes = NumericalUtility.toBigEndianBytes( theLong, 8 );
		eLong = NumericalUtility.toLongFromBigEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theLong + "-?->" + eLong );
		}
		assertEquals( theLong, eLong );
	}

	@Test
	public void testToFromLittleEndianBytes() {
		byte[] eBytes;
		int eValue;
		for ( int i = -1024; i < 1024; i++ ) {
			eBytes = NumericalUtility.toLittleEndianBytes( i, 8 );
			eValue = NumericalUtility.toIntFromLittleEndianBytes( eBytes );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + i + "-?->" + eValue );
			}
			assertEquals( i, eValue );
		}
		// Integer MAX & MIN |-->
		int theValue = Integer.MAX_VALUE;
		eBytes = NumericalUtility.toLittleEndianBytes( theValue, 8 );
		eValue = NumericalUtility.toIntFromLittleEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theValue + "-?->" + eValue );
		}
		assertEquals( theValue, eValue );
		theValue = Integer.MIN_VALUE;
		eBytes = NumericalUtility.toLittleEndianBytes( theValue, 8 );
		eValue = NumericalUtility.toIntFromLittleEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theValue + "-?->" + eValue );
		}
		assertEquals( theValue, eValue );
		// Long MAX & MIN |-->
		long theLong = Long.MAX_VALUE;
		eBytes = NumericalUtility.toLittleEndianBytes( theLong, 8 );
		long eLong = NumericalUtility.toLongFromLittleEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theLong + "-?->" + eLong );
		}
		assertEquals( theLong, eLong );
		theLong = Long.MIN_VALUE;
		eBytes = NumericalUtility.toLittleEndianBytes( theLong, 8 );
		eLong = NumericalUtility.toLongFromLittleEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theLong + "-?->" + eLong );
		}
		assertEquals( theLong, eLong );
	}

	@Test
	public void testFloatToFromBigEndianBytes() {
		byte[] eBytes;
		float eValue;
		for ( float i = 0; i < 1024; i += 1.3337 ) {
			eBytes = NumericalUtility.toBigEndianBytes( i );
			eValue = NumericalUtility.toFloatFromBigEndianBytes( eBytes );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + i + "-?->" + eValue );
			}
			assertEquals( i, eValue );
		}
		// Float MAX & MIN |-->
		float theValue = Float.MAX_VALUE;
		eBytes = NumericalUtility.toBigEndianBytes( theValue );
		eValue = NumericalUtility.toFloatFromBigEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theValue + "-?->" + eValue );
		}
		assertEquals( theValue, eValue );
		theValue = Float.MIN_VALUE;
		eBytes = NumericalUtility.toBigEndianBytes( theValue );
		eValue = NumericalUtility.toFloatFromBigEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theValue + "-?->" + eValue );
		}
		assertEquals( theValue, eValue );
	}

	@Test
	public void testFloatToFromLittleEndianBytes() {
		byte[] eBytes;
		float eValue;
		for ( float i = 0; i < 1024; i += 1.3337 ) {
			eBytes = NumericalUtility.toLittleEndianBytes( i );
			eValue = NumericalUtility.toFloatFromLittleEndianBytes( eBytes );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + i + "-?->" + eValue );
			}
			assertEquals( i, eValue );
		}
		// Float MAX & MIN |-->
		float theValue = Float.MAX_VALUE;
		eBytes = NumericalUtility.toLittleEndianBytes( theValue );
		eValue = NumericalUtility.toFloatFromLittleEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theValue + "-?->" + eValue );
		}
		assertEquals( theValue, eValue );
		theValue = Float.MIN_VALUE;
		eBytes = NumericalUtility.toLittleEndianBytes( theValue );
		eValue = NumericalUtility.toFloatFromLittleEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theValue + "-?->" + eValue );
		}
		assertEquals( theValue, eValue );
	}

	@Test
	public void testDoubleToFromBigEndianBytes() {
		byte[] eBytes;
		double eValue;
		for ( double i = 0; i < 1024; i += 1.3337 ) {
			eBytes = NumericalUtility.toBigEndianBytes( i );
			eValue = NumericalUtility.toDoubleFromBigEndianBytes( eBytes );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + i + "-?->" + eValue );
			}
			assertEquals( i, eValue );
		}
		// Double MAX & MIN |-->
		double theValue = Double.MAX_VALUE;
		eBytes = NumericalUtility.toBigEndianBytes( theValue );
		eValue = NumericalUtility.toDoubleFromBigEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theValue + "-?->" + eValue );
		}
		assertEquals( theValue, eValue );
		theValue = Double.MIN_VALUE;
		eBytes = NumericalUtility.toBigEndianBytes( theValue );
		eValue = NumericalUtility.toDoubleFromBigEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theValue + "-?->" + eValue );
		}
		assertEquals( theValue, eValue );
	}

	@Test
	public void testDoubleToFromLittleEndianBytes() {
		byte[] eBytes;
		double eValue;
		for ( double i = 0; i < 1024; i += 1.3337 ) {
			eBytes = NumericalUtility.toLittleEndianBytes( i );
			eValue = NumericalUtility.toDoubleFromLittleEndianBytes( eBytes );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + i + "-?->" + eValue );
			}
			assertEquals( i, eValue );
		}
		// Double MAX & MIN |-->
		double theValue = Double.MAX_VALUE;
		eBytes = NumericalUtility.toLittleEndianBytes( theValue );
		eValue = NumericalUtility.toDoubleFromLittleEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theValue + "-?->" + eValue );
		}
		assertEquals( theValue, eValue );
		theValue = Double.MIN_VALUE;
		eBytes = NumericalUtility.toLittleEndianBytes( theValue );
		eValue = NumericalUtility.toDoubleFromLittleEndianBytes( eBytes );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( NumericalUtility.toHexString( " ", eBytes ) + " = " + theValue + "-?->" + eValue );
		}
		assertEquals( theValue, eValue );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private String toRandomText( int aLength ) {
		String eText = UUID.randomUUID().toString();
		while ( eText.length() < aLength ) {
			eText = eText + UUID.randomUUID().toString();
		}
		return eText.substring( 0, aLength );
	}
}
