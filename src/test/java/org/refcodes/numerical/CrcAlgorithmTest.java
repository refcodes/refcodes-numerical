// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

import static org.junit.jupiter.api.Assertions.*;
import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Text;

/**
 *
 */
public class CrcAlgorithmTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testNoBitError() {
		final String theValue1 = Text.ARECIBO_MESSAGE + "A";
		final String theOtherValue1 = Text.ARECIBO_MESSAGE + "A";
		long eChecksum;
		long eOtherChecksum;
		byte[] eBytes;
		for ( CrcStandard eCrcChecksum : CrcStandard.values() ) {
			eChecksum = eCrcChecksum.toCrcChecksum( theValue1.getBytes( StandardCharsets.UTF_8 ) );
			eOtherChecksum = eCrcChecksum.toCrcChecksum( theOtherValue1.getBytes( StandardCharsets.UTF_8 ) );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eCrcChecksum.getName() + ": " + Long.toHexString( eChecksum ) + " -?-> " + Long.toHexString( eOtherChecksum ) );
			}
			assertEquals( eChecksum, eOtherChecksum );
			assertTrue( Long.toHexString( eChecksum ).length() <= ( eCrcChecksum.getCrcSize().getCrcWidth() * 2 ) );
			assertTrue( Long.toHexString( eOtherChecksum ).length() <= ( eCrcChecksum.getCrcSize().getCrcWidth() * 2 ) );
			eBytes = eCrcChecksum.toCrcBytes( theValue1.getBytes( StandardCharsets.UTF_8 ), Endianess.LITTLE );
			assertEquals( eChecksum, Endianess.LITTLE.toUnsignedLong( eBytes ) );
		}
	}

	@Test
	public void testOneBitError() {
		final String theValue1 = Text.ARECIBO_MESSAGE + "D";
		final String theOtherValue1 = Text.ARECIBO_MESSAGE + "E";
		long eChecksum;
		long eOtherChecksum;
		byte[] eBytes;
		for ( CrcStandard eCrcChecksum : CrcStandard.values() ) {
			eChecksum = eCrcChecksum.toCrcChecksum( theValue1.getBytes( StandardCharsets.UTF_8 ) );
			eOtherChecksum = eCrcChecksum.toCrcChecksum( theOtherValue1.getBytes( StandardCharsets.UTF_8 ) );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eCrcChecksum.getName() + ": " + Long.toHexString( eChecksum ) + " -?-> " + Long.toHexString( eOtherChecksum ) );
			}
			assertNotEquals( eChecksum, eOtherChecksum );
			assertTrue( Long.toHexString( eChecksum ).length() <= ( eCrcChecksum.getCrcSize().getCrcWidth() * 2 ) );
			assertTrue( Long.toHexString( eOtherChecksum ).length() <= ( eCrcChecksum.getCrcSize().getCrcWidth() * 2 ) );
			eBytes = eCrcChecksum.toCrcBytes( theValue1.getBytes( StandardCharsets.UTF_8 ), Endianess.LITTLE );
			assertEquals( eChecksum, Endianess.LITTLE.toUnsignedLong( eBytes ) );
		}
	}

	@Test
	public void testTwoBitError() {
		final String theValue1 = Text.ARECIBO_MESSAGE + "C";
		final String theOtherValue1 = Text.ARECIBO_MESSAGE + "F";
		long eChecksum;
		long eOtherChecksum;
		byte[] eBytes;
		for ( CrcStandard eCrcChecksum : CrcStandard.values() ) {
			eChecksum = eCrcChecksum.toCrcChecksum( theValue1.getBytes( StandardCharsets.UTF_8 ) );
			eOtherChecksum = eCrcChecksum.toCrcChecksum( theOtherValue1.getBytes( StandardCharsets.UTF_8 ) );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eCrcChecksum.getName() + ": " + Long.toHexString( eChecksum ) + " -?-> " + Long.toHexString( eOtherChecksum ) );
			}
			assertNotEquals( eChecksum, eOtherChecksum );
			assertTrue( Long.toHexString( eChecksum ).length() <= ( eCrcChecksum.getCrcSize().getCrcWidth() * 2 ) );
			assertTrue( Long.toHexString( eOtherChecksum ).length() <= ( eCrcChecksum.getCrcSize().getCrcWidth() * 2 ) );
			eBytes = eCrcChecksum.toCrcBytes( theValue1.getBytes( StandardCharsets.UTF_8 ), Endianess.LITTLE );
			assertEquals( eChecksum, Endianess.LITTLE.toUnsignedLong( eBytes ) );
		}
	}

	@Test
	public void testRandomLongChecksum() {
		long eChecksum;
		long eOtherChecksum;
		byte[] eBytes;
		for ( CrcStandard eCrcChecksum : CrcStandard.values() ) {
			long eValue;
			for ( int i = 0; i < 1024; i++ ) {
				eValue = (long) ( Math.random() * Long.MAX_VALUE );
				eChecksum = eCrcChecksum.toCrcChecksum( NumericalUtility.toBytes( eValue ) );
				eOtherChecksum = eCrcChecksum.toCrcChecksum( NumericalUtility.toBytes( eValue ) );
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.print( eCrcChecksum.getName() + ": " + eValue + " = " + Long.toHexString( eChecksum ) + " -?-> " + Long.toHexString( eOtherChecksum ) );
				}
				assertEquals( eChecksum, eOtherChecksum );
				assertTrue( Long.toHexString( eChecksum ).length() <= ( eCrcChecksum.getCrcSize().getCrcWidth() * 2 ) );
				eBytes = eCrcChecksum.toCrcBytes( NumericalUtility.toBytes( eValue ), Endianess.LITTLE );
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( " {" + NumericalUtility.toHexString( ", ", eBytes ) + "}" );
				}
				assertEquals( eChecksum, Endianess.LITTLE.toUnsignedLong( eBytes ) );
			}
		}
	}

	@Test
	public void testCrcCategories() {
		for ( CrcSize eCrcCategory : CrcSize.values() ) {
			for ( CrcStandard eCrcAlgorithm : eCrcCategory.getCrcAlgorithms() ) {
				if ( IS_LOG_TESTS_ENABLED ) {
					System.out.println( eCrcCategory + " <-- " + eCrcAlgorithm );
				}
				assertTrue( eCrcAlgorithm.name().startsWith( eCrcCategory.name() ) );
			}
		}
	}

	@Test
	public void testCrcAlgorithms() {
		for ( CrcStandard eCrcAlgorithm : CrcStandard.values() ) {
			final CrcSize theCrcCategory = eCrcAlgorithm.getCrcSize();
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( eCrcAlgorithm + " --> " + theCrcCategory );
			}
			assertTrue( eCrcAlgorithm.name().startsWith( theCrcCategory.name() ) );
		}
	}
}
