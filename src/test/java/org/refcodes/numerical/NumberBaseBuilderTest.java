// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.numerical;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * The Class NumberBaseBuilderTest.
 *
 * @author steiner
 */
public class NumberBaseBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testToNumberBase10Value() {
		final int theNumberBase = 10;
		final int theNumber = 2009;
		final String theExpextedValue = Integer.toString( theNumber );
		final BaseConverterBuilder theNumberBaseBuilder = new BaseConverterBuilder().withNumberBase( theNumberBase );
		// To number base value:
		final String theValue = theNumberBaseBuilder.toDigits( theNumber );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Converting number <" + theNumber + "> using number base <" + theNumberBase + "> to <" + theValue + ">" );
		}
		assertEquals( theExpextedValue, theValue );
		// Back to number:
		final long theLoopbackNumber = theNumberBaseBuilder.toNumber( theValue );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Converting number base value <" + theValue + "> using number base <" + theNumberBase + "> to <" + theLoopbackNumber + ">" );
		}
		assertEquals( theNumber, theLoopbackNumber );
	}

	@Test
	public void testToNumberBase16Value() {
		final int theNumberBase = 16;
		final int theNumber = 65535;
		final String theExpextedValue = "FFFF";
		final BaseConverterBuilder theNumberBaseBuilder = new BaseConverterBuilder().withNumberBase( theNumberBase );
		// To number base value:
		final String theValue = theNumberBaseBuilder.toDigits( theNumber );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Converting number <" + theNumber + "> using number base <" + theNumberBase + "> to <" + theValue + ">" );
		}
		assertEquals( theExpextedValue, theValue );
		// Back to number:
		final long theLoopbackNumber = theNumberBaseBuilder.toNumber( theValue );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Converting number base value <" + theValue + "> using number base <" + theNumberBase + "> to <" + theLoopbackNumber + ">" );
		}
		assertEquals( theNumber, theLoopbackNumber );
	}
}
